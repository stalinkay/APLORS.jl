using Pkg;
Pkg.add([
    "CSV",
    "DataFrames",
    "Flux",
    "Images",
    "Juno",
    "MLDataUtils",
    "MLJ",
    "MPI",
    "Plots",
    "Revise",
    "JuliaFormatter",
]); # Add FluxUtils
