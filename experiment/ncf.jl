### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ f32cf18a-0296-11ec-15dd-73db418c15e7
begin
	using Revise
	using Pkg
	Pkg.activate("..")
	using APLORS
	using APLORS.Models: Dot, Sum
	import LinearAlgebra: dot
	using Flux: ADAM, BatchNorm, Chain, DataLoader, Dense, Dropout, Embedding, Parallel, activations, early_stopping, logitcrossentropy, modules,  mse, onecold, onehotbatch, OneHotMatrix, outdims, params, relu, stop, train!, @epochs, @functor
	using Random
	using Statistics: mean
	using PrettyTables
end

# ╔═╡ 2a23f23c-0b50-4af4-b43a-007fc0f88a5a
USE_CLASSIFICATION = false

# ╔═╡ 9651ed69-7a39-4168-9be6-11f2b4ab4904
USE_BATCH_NORM = true

# ╔═╡ 52ab6d0d-d777-4df5-af95-ab0db20cfa8a
EMBEDDING_SIZE = 32

# ╔═╡ 07cb224f-e8af-42f8-8e33-88f842de2d9a
USE_L2 = true # Doesn't work well with nmf, use ncf to combine gmf and ncf; ncf converges faster than ncfparallel

# ╔═╡ 00f9b5b2-6779-4bf9-ac5a-152177a5599e
DROPOUT_RATE = 0.2

# ╔═╡ 9f4a384b-8b27-4f30-abbd-89317435d17a
MODEL_NAME = "ncf" #gmf, nmf, ncfparallel or ncf

# ╔═╡ b60769ac-f665-41ae-a5a8-424d2336c2b2
MLP_EXTRA_LAYERS = 1

# ╔═╡ 6b89e10e-8a11-4ca5-b6c7-c22bc4ae0b08
OUTPUT_SIZE = MODEL_NAME == "gmf" || MODEL_NAME == "nmf" ? EMBEDDING_SIZE : EMBEDDING_SIZE * 2

# ╔═╡ 514002d9-5d57-4811-b255-af41edb66525
Random.seed!(7); X = rand(1:1_000, 2, 1_000)

# ╔═╡ fa1fa8cc-1659-43d8-ab55-241c675f26ef
Random.seed!(7); y = round.(Int64, sum(X.^2, dims = 1)); y = abs.(y); y = rand(1:6, 1_000); y = USE_CLASSIFICATION ? onehotbatch(y, sort(unique(y))) : y' |> Matrix

# ╔═╡ d7dbd9df-92d9-43e6-9c39-9f42e830bdb9
Random.seed!(7); StudentEncoder = Chain(Dense(OUTPUT_SIZE * 2, OUTPUT_SIZE * 2, relu), Dense(OUTPUT_SIZE * 2, OUTPUT_SIZE * 2, relu), Dense(OUTPUT_SIZE * 2, OUTPUT_SIZE, relu))

# ╔═╡ 0fa0b855-f542-4900-b0ef-792716d6b742
Random.seed!(7); StudentDecoder = Chain(Dense(OUTPUT_SIZE, OUTPUT_SIZE * 2, relu), Dense(OUTPUT_SIZE * 2, OUTPUT_SIZE * 2, relu), Dense(OUTPUT_SIZE * 2, OUTPUT_SIZE * 2, relu))

# ╔═╡ 987973d8-d6c1-4ea9-b3f8-7688303ddc4f
Random.seed!(7); StudentAEModel = Chain(StudentEncoder, StudentDecoder)

# ╔═╡ a38558bc-50f7-42dc-a9e5-231297bda2af
Random.seed!(7); student_embeddings_ae = Embedding(1_000, OUTPUT_SIZE * 2)(X[1, :]); student_data = DataLoader((student_embeddings_ae, student_embeddings_ae), batchsize = 32)

# ╔═╡ 9c76cac5-c566-45c5-882b-ad3ce2719089
AELoss(x, y) = sqrt(mse(StudentAEModel(x), y))

# ╔═╡ 9ef1c4ab-b704-4e69-a12b-1afb530dfe9d
function AEAccuracy(x, y)
	sum(StudentAEModel(x) .== y) / length(y)
end

# ╔═╡ cd480130-cfeb-47d6-8a93-edf1fc86f1ca
# for epoch in 1:100
# 	train!(AELoss, params(StudentEncoder), student_data, ADAM())
# 	# print("AEAccuracy: ", AEAccuracy(student_embeddings_ae, student_embeddings_ae), pretty_table(StudentAEModel(student_embeddings_ae)), "\n")
# 	# pretty_table(StudentAEModel(student_embeddings_ae)[:, 1])
# 	# pretty_table(student_embeddings_ae[:, 1])
# 	print("AELoss: ", AELoss(student_embeddings_ae, student_embeddings_ae), "\n")
# end

# ╔═╡ 786d0264-9c2a-462a-943a-8b57e6fbec07
Random.seed!(7); student_embedding = Embedding(1_000, EMBEDDING_SIZE)

# ╔═╡ e7f1070a-1b0f-45bc-9779-da697135b980
Random.seed!(7); learning_object_embedding = Embedding(1_000, EMBEDDING_SIZE)

# ╔═╡ 33ca819f-d01a-4c23-b794-4cbdf278cf44
@time learning_object_embedding(OneHotMatrix([100, 100, 1], 1_000))

# ╔═╡ e5d4c319-cc8c-453a-a61d-52157a3f9c83
@time learning_object_embedding(X[2, :])

# ╔═╡ 1a594604-a829-47e0-a436-0aea85c6ee4f
vecdot(x, y) = dot.(x, y)

# ╔═╡ adc189df-ecd9-4310-becf-1e522498d818
begin
	Random.seed!(7)
	# MLP = Chain(
	# 	Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE * 2),
	# 	BatchNorm(EMBEDDING_SIZE * 2, relu),
	# 	Dropout(DROPOUT_RATE),
	# 	Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE * 2, relu),
	# 	BatchNorm(EMBEDDING_SIZE * 2, relu),
	# 	Dropout(DROPOUT_RATE),
	# 	Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE, relu),
	# 	BatchNorm(EMBEDDING_SIZE, relu),
	# 	Dropout(DROPOUT_RATE),
	# )
	dense_start_layer = Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE * 2)
	dense_middle_layer = Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE * 2, relu)
	dense_end_layer = Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE, relu)

	batch_layer = BatchNorm(EMBEDDING_SIZE * 2, relu)
	batch_end_layer = BatchNorm(EMBEDDING_SIZE, relu)

	dropout_layer = Dropout(DROPOUT_RATE)

	MLP_LAYERS = USE_BATCH_NORM ? [dense_start_layer, batch_layer, dropout_layer] : [dense_start_layer, dropout_layer]

	for i in 1:MLP_EXTRA_LAYERS
		USE_BATCH_NORM ? push!(MLP_LAYERS, dense_middle_layer, batch_layer, dropout_layer) : push!(MLP_LAYERS, dense_middle_layer, dropout_layer)
	end

	MLP = USE_BATCH_NORM ? Chain(MLP_LAYERS..., dense_end_layer, batch_end_layer, dropout_layer) : Chain(MLP_LAYERS..., dense_end_layer, dropout_layer) |> APLORS.Models.gpu_or_cpu
end

# ╔═╡ ffff9f3d-b527-465c-a4e4-bdadb755a500
begin
	struct NCF
		gmf
		nmf
	end
	(m::NCF)(x) = vcat(m.gmf(x), m.nmf(x))
	@functor NCF
end

# ╔═╡ f1cd2af6-3abe-4e5a-8aba-62e6e1921b0f
begin
	struct Vcat
		a
		b
	end
	function work(m, x)
		print("work: ", x)
		vcat(m.a(x[1]), m.b(x[2]))
	end
	(m::Vcat)(x) = work(m, x)
	@functor Vcat
end

# ╔═╡ f9bbb1dd-ef33-4eb3-88fa-a50ad212578a
GMFModel = Parallel(vecdot, student_embedding, learning_object_embedding)

# ╔═╡ f73fea2a-dd0a-4f30-b0f9-4b0dd6e4f764
NMFModel = Chain(Parallel(vcat, student_embedding, learning_object_embedding), MLP)

# ╔═╡ d49f16c3-f7d9-4064-93a5-f8a9d142916e
NCFParallelModel = Parallel(vcat, GMFModel, NMFModel)

# ╔═╡ 5340c930-8841-46f4-af2c-9dcb7311bdc2
NCFModel = NCF(GMFModel, NMFModel)

# ╔═╡ 0d33841b-af6c-40e1-a465-8b96a5880b13
begin
	Random.seed!(7)
	model = GMFModel
	model = MODEL_NAME == "nmf" ? NMFModel : model
	model = MODEL_NAME == "ncfparallel" ? NCFParallelModel : model
	model = MODEL_NAME == "ncf" ? NCFModel : model
	model = Chain(model, USE_CLASSIFICATION ? Dense(OUTPUT_SIZE, 6) : Dense(OUTPUT_SIZE, 1))
end

# ╔═╡ 70703e66-8d3c-4527-8f3f-227d8d53246d
GMFModel(([X[1, 1]], [X[2, 1]]))

# ╔═╡ 5cb335bb-f6e2-4bdc-b06f-5d63c43e87ca
NMFModel(([X[1, 1]], [X[2, 1]]))

# ╔═╡ f31e3a2e-4af6-4e50-8a1c-4594e73778cc
student_embedding([X[1, 1]])

# ╔═╡ 5250f8d0-1ff3-4cdd-99c1-240c872e300d
learning_object_embedding([X[2, 1]])

# ╔═╡ 18c45d25-bf79-441a-adce-433e657a1e9e
NCFParallelModel(([X[1, 1]], [X[2, 1]]))

# ╔═╡ 9f026c43-68ca-4739-acbc-c841c4cc36ff
NCFModel(([X[1, 1]], [X[2, 1]]))

# ╔═╡ cb2df9a8-46a8-4628-af26-fa197cc09235
L2(m) = sum(sum(abs2, l.weight) for l in modules(m) if l isa Dense)

# ╔═╡ eca6f23e-e6b7-4016-b491-9a36de12d7ba
sqnorm(x) = sum(abs2, x)

# ╔═╡ 47ca1e9d-2340-4154-b1f4-cec2a7bff0ec
accuracy_binary(x, y) = sum(round.(Int8, model(x)) .== y) / length(y)

# ╔═╡ 59c29d60-8f5d-4030-9391-2d5433f4a379
accuracy_classification(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ 0216c56f-2f90-44ab-8a86-16c76945a290
loss_classification(x, y) = USE_L2 ? logitcrossentropy(model(x), y) + L2(model) : logitcrossentropy(model(x), y)

# ╔═╡ e9110816-d2bd-49e1-b39e-34dfb22efb30
loss_binary(x, y) = USE_L2 ? sqrt(mse(model(x), y)) + L2(model) : sqrt(mse(model(x), y))

# ╔═╡ 02d9ac87-22b3-4f49-b144-7a24691579c4
accuracy = USE_CLASSIFICATION ? accuracy_classification : accuracy_binary

# ╔═╡ f6ff5dc1-2034-4b70-8807-7bb1dd160654
loss = USE_CLASSIFICATION ? loss_classification : loss_binary

# ╔═╡ afbc6c84-5805-4626-aad5-9c6b411243ae
acc = () -> accuracy((X[1, :], X[2, :]), y) < 0.5

# ╔═╡ 567aaa30-85a0-41ab-9baf-cb9317d335a7
es = early_stopping(acc, 1)

# ╔═╡ 19da6dd3-669d-4e85-9bd3-278b1ba4705b
opt = ADAM(0.01)

# ╔═╡ 2cc6f73a-8fef-4f9c-9623-50162117e683
@time data = DataLoader(((X[1, :], X[2, :]), y), batchsize = 32)

# ╔═╡ a70eb990-8694-4854-bdb6-3ac03b8dde1d
ps = params(model)

# ╔═╡ 31485718-b9c2-470b-9384-6eb3f2b913ff
function custom_train!(loss, ps, data, opt)
	# for d in data
		# x, y = d
		train!(loss, ps, data, opt)
		# es() && break
	# end
	print("loss: ", loss((X[1, :], X[2, :]), y), "\n")
	print("accuracy: ", accuracy((X[1, :], X[2, :]), y), "\n")
end

# ╔═╡ 26723bca-9fba-496d-9197-00e9a942f9cb
@time @epochs 200 custom_train!(loss, ps, data, opt)

# ╔═╡ 6558e36d-f9be-4b32-ab93-d6d7a4af2113
accuracy((X[1, :], X[2, :]), y)

# ╔═╡ 5c400877-7a46-4015-aacf-b033d1d02cea
(X[1, 1:5], X[2, 1:5])

# ╔═╡ c85f835e-3e0c-4d0b-907a-3ba6024172b3
y

# ╔═╡ d4ced597-28ad-4e69-a22a-1cbcf260003e
USE_CLASSIFICATION ? onehotbatch(onecold(model((X[1, :], X[2, :]))), 1:6) : round.(Int8, model((X[1, :], X[2, :])))

# ╔═╡ 12d76d6a-ea58-43d7-8d83-604fb2675ca7
USE_CLASSIFICATION ? onecold(y[:, :]) : y[:, :]

# ╔═╡ acc6cef3-6880-426c-ba3a-8d566a2b04a0
USE_CLASSIFICATION ? onecold(model((X[1, :], X[2, :]))) : model((X[1, :], X[2, :]))

# ╔═╡ a0cc42a4-6850-46de-884e-17d1573d2ead
loss((X[1, :], X[2, :]), y[:, :])

# ╔═╡ 5adcba69-5b66-49bc-bc40-e42a6a3b3800
USE_CLASSIFICATION ? mean(onecold(model((X[1, :], X[2, :]))) .== onecold(y)) : round.(Int8, model((X[1, :], X[2, :]))) .== y

# ╔═╡ 8982f494-74c4-4018-b315-8a2c27dc222b
L2(model)

# ╔═╡ 8faf345d-e816-4bbf-afe1-688fbbefd7c9
sum(sqnorm, params(GMFModel))

# ╔═╡ 6ea2018d-2623-44a3-a9e8-29912ffea5dc
sum(sqnorm, params(NMFModel))

# ╔═╡ f208b4d7-00db-4ea9-a473-2129107ab8d6
sum(sqnorm, params(NCFParallelModel))

# ╔═╡ 4ebbd26b-9595-4848-be13-81cd0d883ce7
sum(sqnorm, params(NCFModel))

# ╔═╡ fc021171-f751-4c7f-aab6-0c917e485d0a
ans = activations(model, (X[1, :], X[2, :]))

# ╔═╡ d029faec-470a-4e55-b402-e6ace07d8e75
sum(sqnorm, ans)

# ╔═╡ Cell order:
# ╠═f32cf18a-0296-11ec-15dd-73db418c15e7
# ╠═2a23f23c-0b50-4af4-b43a-007fc0f88a5a
# ╠═9651ed69-7a39-4168-9be6-11f2b4ab4904
# ╠═52ab6d0d-d777-4df5-af95-ab0db20cfa8a
# ╠═07cb224f-e8af-42f8-8e33-88f842de2d9a
# ╠═00f9b5b2-6779-4bf9-ac5a-152177a5599e
# ╠═9f4a384b-8b27-4f30-abbd-89317435d17a
# ╠═b60769ac-f665-41ae-a5a8-424d2336c2b2
# ╠═6b89e10e-8a11-4ca5-b6c7-c22bc4ae0b08
# ╠═514002d9-5d57-4811-b255-af41edb66525
# ╠═fa1fa8cc-1659-43d8-ab55-241c675f26ef
# ╠═d7dbd9df-92d9-43e6-9c39-9f42e830bdb9
# ╠═0fa0b855-f542-4900-b0ef-792716d6b742
# ╠═987973d8-d6c1-4ea9-b3f8-7688303ddc4f
# ╠═a38558bc-50f7-42dc-a9e5-231297bda2af
# ╠═9c76cac5-c566-45c5-882b-ad3ce2719089
# ╠═9ef1c4ab-b704-4e69-a12b-1afb530dfe9d
# ╠═cd480130-cfeb-47d6-8a93-edf1fc86f1ca
# ╠═786d0264-9c2a-462a-943a-8b57e6fbec07
# ╠═e7f1070a-1b0f-45bc-9779-da697135b980
# ╠═33ca819f-d01a-4c23-b794-4cbdf278cf44
# ╠═e5d4c319-cc8c-453a-a61d-52157a3f9c83
# ╠═1a594604-a829-47e0-a436-0aea85c6ee4f
# ╠═adc189df-ecd9-4310-becf-1e522498d818
# ╠═ffff9f3d-b527-465c-a4e4-bdadb755a500
# ╠═f1cd2af6-3abe-4e5a-8aba-62e6e1921b0f
# ╠═f9bbb1dd-ef33-4eb3-88fa-a50ad212578a
# ╠═f73fea2a-dd0a-4f30-b0f9-4b0dd6e4f764
# ╠═d49f16c3-f7d9-4064-93a5-f8a9d142916e
# ╠═5340c930-8841-46f4-af2c-9dcb7311bdc2
# ╠═0d33841b-af6c-40e1-a465-8b96a5880b13
# ╠═70703e66-8d3c-4527-8f3f-227d8d53246d
# ╠═5cb335bb-f6e2-4bdc-b06f-5d63c43e87ca
# ╠═f31e3a2e-4af6-4e50-8a1c-4594e73778cc
# ╠═5250f8d0-1ff3-4cdd-99c1-240c872e300d
# ╠═18c45d25-bf79-441a-adce-433e657a1e9e
# ╠═9f026c43-68ca-4739-acbc-c841c4cc36ff
# ╠═cb2df9a8-46a8-4628-af26-fa197cc09235
# ╠═eca6f23e-e6b7-4016-b491-9a36de12d7ba
# ╠═47ca1e9d-2340-4154-b1f4-cec2a7bff0ec
# ╠═59c29d60-8f5d-4030-9391-2d5433f4a379
# ╠═0216c56f-2f90-44ab-8a86-16c76945a290
# ╠═e9110816-d2bd-49e1-b39e-34dfb22efb30
# ╠═02d9ac87-22b3-4f49-b144-7a24691579c4
# ╠═f6ff5dc1-2034-4b70-8807-7bb1dd160654
# ╠═afbc6c84-5805-4626-aad5-9c6b411243ae
# ╠═567aaa30-85a0-41ab-9baf-cb9317d335a7
# ╠═19da6dd3-669d-4e85-9bd3-278b1ba4705b
# ╠═2cc6f73a-8fef-4f9c-9623-50162117e683
# ╠═a70eb990-8694-4854-bdb6-3ac03b8dde1d
# ╠═31485718-b9c2-470b-9384-6eb3f2b913ff
# ╠═26723bca-9fba-496d-9197-00e9a942f9cb
# ╠═6558e36d-f9be-4b32-ab93-d6d7a4af2113
# ╠═5c400877-7a46-4015-aacf-b033d1d02cea
# ╠═c85f835e-3e0c-4d0b-907a-3ba6024172b3
# ╠═d4ced597-28ad-4e69-a22a-1cbcf260003e
# ╠═12d76d6a-ea58-43d7-8d83-604fb2675ca7
# ╠═acc6cef3-6880-426c-ba3a-8d566a2b04a0
# ╠═a0cc42a4-6850-46de-884e-17d1573d2ead
# ╠═5adcba69-5b66-49bc-bc40-e42a6a3b3800
# ╠═8982f494-74c4-4018-b315-8a2c27dc222b
# ╠═8faf345d-e816-4bbf-afe1-688fbbefd7c9
# ╠═6ea2018d-2623-44a3-a9e8-29912ffea5dc
# ╠═f208b4d7-00db-4ea9-a473-2129107ab8d6
# ╠═4ebbd26b-9595-4848-be13-81cd0d883ce7
# ╠═fc021171-f751-4c7f-aab6-0c917e485d0a
# ╠═d029faec-470a-4e55-b402-e6ace07d8e75
