### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 39b27fa3-f554-443d-9e72-17a88295e465
using Lathe, Flux, LinearAlgebra, PrettyTables, DataFrames, TextAnalysis

# ╔═╡ 6500d220-865f-4db9-8a67-1932ebe7eacf
using MLJ: coerce!, coerce, machine, transform, Standardizer, fit!, OneHotEncoder, Multiclass

# ╔═╡ 22acce8f-8a4d-41db-a5cd-aa58b271c8d8
using Statistics: mean

# ╔═╡ cf8fb224-c1d9-11eb-0e3c-77e51b18370e
import APLORS.DataPattern: read_data_frame

# ╔═╡ 01fdff6b-0ab6-4b13-a7d6-e4634caaf863
course_df = read_data_frame("release-candidate/courses.csv")

# ╔═╡ dc15a17d-d4d1-4003-bec1-0336a941b899
course_gdf = groupby(course_df, :course_id)

# ╔═╡ 84ce3680-0988-4ea1-a770-afb385526d4a
course_prerequisite_df = read_data_frame("release-candidate/coursePrerequisites.csv");

# ╔═╡ e2f5286c-4de3-4336-827b-a6ae89c49cf0
get_course_prequisites(c) = unique(course_prerequisite_df[course_prerequisite_df[!, :course_id] .== c,  :prerequisite])

# ╔═╡ 6c5eed7d-1bb0-4a90-9474-d567784364ac
course_prerequisites_df = combine(course_gdf, :course_id .=> (c -> [get_course_prequisites(i) for i in c]) .=> :prerequisite)

# ╔═╡ e76d3615-8e36-4389-bbfe-dc4500f1fd89
course_df[!, :course_prerequisite_id] .= course_prerequisites_df[!, :prerequisite]; course_df

# ╔═╡ 7043cf01-9076-4e9c-97a6-fc5fd686f6b3
topic_df = read_data_frame("release-candidate/topics.csv")

# ╔═╡ 751d6f69-d785-40c3-bc80-fc74e631287e
topic_gdf = groupby(topic_df, :topic_id)

# ╔═╡ f4f119f1-5b99-46d2-9857-b083cb44384b
topic_prerequisite_df = read_data_frame("release-candidate/topicPrerequisites.csv");

# ╔═╡ e6764675-eda7-41fd-84db-ddf2ad84f687
get_topic_prequisites(t) = unique(topic_prerequisite_df[topic_prerequisite_df.topic_id .== t,  :prerequisite])

# ╔═╡ 07240655-ab84-4e69-94ea-4aff3845ae75
topic_prerequisites_df = combine(topic_gdf, :topic_id .=> (t -> [get_topic_prequisites(i) for i in t]) .=> :prerequisite)

# ╔═╡ 8914fcfa-dffb-4c98-9b5a-b0b39b46e9a2
topic_df[!, :topic_prerequisite_id] .= topic_prerequisites_df[!, :prerequisite]; topic_df

# ╔═╡ e068e17a-da59-4d94-b942-596b7ba14a0a
learning_object_df = read_data_frame("release-candidate/learningObjects.csv"); learning_object_df = innerjoin(learning_object_df, course_df, on = :course_id) |> DataFrame; learning_object_df = innerjoin(learning_object_df, topic_df, on = [:topic_id, :course_id]) |> DataFrame; #learning_object_df = first(learning_object_df) |> DataFrame

# ╔═╡ 6a530aac-edf8-419c-b5cd-7fc3382b91f6
unique_course_prerequisites = sort(unique(reduce(vcat, course_prerequisites_df.prerequisite)))

# ╔═╡ 5d7393ab-61e5-4654-a1fe-937eaf6a57a1
unique_topic_prerequisites = sort(unique(reduce(vcat, topic_prerequisites_df.prerequisite)))

# ╔═╡ b9f017de-8eb5-4caa-bed3-b22708f92066
DataFrames.transform!(learning_object_df, :course_prerequisite_id .=> [ByRow(v -> x in v) for x in unique_course_prerequisites] .=> Symbol.(:course_prerequisite_id__, unique_course_prerequisites))

# ╔═╡ a084e435-bd6f-4bbf-a569-1abe7392aee5
student_df = read_data_frame("release-candidate/students.csv"); student_df = innerjoin(student_df, course_df, on = :course_id) |> DataFrame

# ╔═╡ 7a4268a8-d40d-4382-81f0-c4362e196c12
student_gdf = groupby(student_df, :student_id)

# ╔═╡ 543ed902-f85b-4582-8d1a-1e1fd5b060c4
student_preferred_topics_df = read_data_frame("release-candidate/studentPreferredTopics.csv")

# ╔═╡ a42bc0d4-7325-4c3e-a923-fbecc9b7d307
student_course_enrollments_df = read_data_frame("release-candidate/studentCourseEnrollments.csv")

# ╔═╡ edbd92cd-d7cf-4ba0-b377-0f2d4671b32d
student_preferred_topics_gdf = groupby(student_preferred_topics_df, :student_id)

# ╔═╡ 6f7002c0-b4a4-43f9-bb22-694759e7857a
get_student_topics(s) = unique(student_preferred_topics_df[student_preferred_topics_df.student_id .== s, :topic_id])

# ╔═╡ dfa5ba36-d6ca-438a-bdba-d6c4115021a5
get_student_course_enrollments(s) = unique(student_course_enrollments_df[student_course_enrollments_df.student_id .== s, :course_id])

# ╔═╡ a33e1cea-ee1a-4c99-a0f8-dd9ea22885f4
preferred_topics_df = combine(student_gdf, :student_id .=> (s -> [get_student_topics(i) for i in s]) .=> :topic_id)

# ╔═╡ 3a9eaeb4-bd27-4fb0-bcf4-4b0973a7f853
course_enrollments_df = combine(student_gdf, :student_id .=> (s -> [get_student_course_enrollments(i) for i in s]) .=> :course_id)

# ╔═╡ a67f963e-5e13-43d0-897e-a8b26bea328b
student_df[!, :topic_id] .= preferred_topics_df[!, :topic_id]; student_df

# ╔═╡ 7fb0e555-0946-40c8-bff2-31ed7c063eac
student_df[!, :course_id] .= course_enrollments_df[!, :course_id]; student_df

# ╔═╡ 74c4adb1-51a5-4dff-b12c-5deaa9346338
unique_topics = sort(unique(reduce(vcat, preferred_topics_df.topic_id)))

# ╔═╡ a807152a-a11a-4f0a-8183-021171d1aa23
unique_enrollments = sort(unique(reduce(vcat, course_enrollments_df.course_id)))

# ╔═╡ 6c1f96fa-7810-4ed6-8c5e-5bbbedf1ab12
DataFrames.transform!(student_df, :topic_id .=> [ByRow(v -> x in v) for x in unique_topics] .=> Symbol.(:topic_id__, unique_topics))

# ╔═╡ d4d2ed12-391a-4ac3-a700-27a1868c5c54
DataFrames.transform!(student_df, :course_id .=> [ByRow(v -> x in v) for x in unique_enrollments] .=> Symbol.(:course_id__, unique_enrollments))

# ╔═╡ 1352f986-bb42-43cd-97c6-8bebb0e2a747
# map(get_student_topics, student_df[:, :student_id])

# ╔═╡ 3286a4bc-63d7-477b-8ab8-a116beb3b9b7
DataFrames.transform(student_df, :student_id => mean)

# ╔═╡ b1920d34-5be6-45c0-94ef-c097e54e23c7
get_student_topics(student_df[:, :student_id][100])

# ╔═╡ ef0da706-e06d-4c57-bef5-1e7b20310595
student_df

# ╔═╡ c091e6b6-d022-4019-b006-0cdb5164a592
student_preferred_topics_gdf

# ╔═╡ 9c39aba9-692b-48da-b0bb-423943b73dac
coerce(preferred_topics_df, :student_id => Multiclass) |> cpu

# ╔═╡ 003dceaa-48d3-4a00-a026-c1a43507c07e
# keys(combine(student_preferred_topics_gdf, :topic_id .=> mean)

# ╔═╡ d37684ee-88a1-4b93-a6fb-9d91a28ac501
lo_cols = names(learning_object_df); student_cols = names(student_df); intersect(lo_cols, student_cols), in(lo_cols)

# ╔═╡ de0f57a6-7f7d-461a-a40d-b1bf23b74c5f
stand_model = Standardizer(); transform(fit!(machine(stand_model, learning_object_df)), learning_object_df)

# ╔═╡ 6fc17cbe-0db0-4407-b761-3c7cfaf7ec04
array_df = DataFrame(A=[1, 1, 2, 2, 3, 4,6,5]); coerce!(array_df, :A => Multiclass); ohe = machine(OneHotEncoder(), array_df);  fit!(ohe), transform(ohe, array_df)

# ╔═╡ 8e4215d9-8975-47c6-9483-0c4c80de593b
# topic_encoding = Flux.onehotbatch(topic_df[!, :topic_id], unique(topic_df[!, :topic_id]))

# ╔═╡ 6e4ef700-47b1-47e8-b3d1-1cc68ffc9f96
# hcat(topic_df, DataFrame(topic_encoding))

# ╔═╡ 41553424-f87f-49d4-a708-d5dfb73c9216
# nrow(DataFrame(Flux.onehotbatch(topic_df[!, :topic_id], unique(topic_df[!, :topic_id]))))

# ╔═╡ a525a083-6dd2-4e8d-a494-d3b9173ce935
# Add missing columns before concatenating dataframes

# for n in unique([propertynames(learning_object_df); propertynames(student_df)]), df in [learning_object_df, student_df]
# 	n in propertynames(df) || (df[!, n] .= missing)
# 	println(n)
# end

# ╔═╡ 9f07b374-bdd3-41ae-a380-41d8cab1e8a5
# length(student_df.student_id)

# ╔═╡ 57166142-f792-4549-8570-784cc9fcb1fa
# vcat(learning_object_df, student_df)

# ╔═╡ Cell order:
# ╠═cf8fb224-c1d9-11eb-0e3c-77e51b18370e
# ╠═39b27fa3-f554-443d-9e72-17a88295e465
# ╠═6500d220-865f-4db9-8a67-1932ebe7eacf
# ╠═22acce8f-8a4d-41db-a5cd-aa58b271c8d8
# ╠═01fdff6b-0ab6-4b13-a7d6-e4634caaf863
# ╠═dc15a17d-d4d1-4003-bec1-0336a941b899
# ╠═84ce3680-0988-4ea1-a770-afb385526d4a
# ╠═e2f5286c-4de3-4336-827b-a6ae89c49cf0
# ╠═6c5eed7d-1bb0-4a90-9474-d567784364ac
# ╠═e76d3615-8e36-4389-bbfe-dc4500f1fd89
# ╠═7043cf01-9076-4e9c-97a6-fc5fd686f6b3
# ╠═751d6f69-d785-40c3-bc80-fc74e631287e
# ╠═f4f119f1-5b99-46d2-9857-b083cb44384b
# ╠═e6764675-eda7-41fd-84db-ddf2ad84f687
# ╠═07240655-ab84-4e69-94ea-4aff3845ae75
# ╠═8914fcfa-dffb-4c98-9b5a-b0b39b46e9a2
# ╠═e068e17a-da59-4d94-b942-596b7ba14a0a
# ╠═6a530aac-edf8-419c-b5cd-7fc3382b91f6
# ╠═5d7393ab-61e5-4654-a1fe-937eaf6a57a1
# ╠═b9f017de-8eb5-4caa-bed3-b22708f92066
# ╠═a084e435-bd6f-4bbf-a569-1abe7392aee5
# ╠═7a4268a8-d40d-4382-81f0-c4362e196c12
# ╠═543ed902-f85b-4582-8d1a-1e1fd5b060c4
# ╠═a42bc0d4-7325-4c3e-a923-fbecc9b7d307
# ╠═edbd92cd-d7cf-4ba0-b377-0f2d4671b32d
# ╠═6f7002c0-b4a4-43f9-bb22-694759e7857a
# ╠═dfa5ba36-d6ca-438a-bdba-d6c4115021a5
# ╠═a33e1cea-ee1a-4c99-a0f8-dd9ea22885f4
# ╠═3a9eaeb4-bd27-4fb0-bcf4-4b0973a7f853
# ╠═a67f963e-5e13-43d0-897e-a8b26bea328b
# ╠═7fb0e555-0946-40c8-bff2-31ed7c063eac
# ╠═74c4adb1-51a5-4dff-b12c-5deaa9346338
# ╠═a807152a-a11a-4f0a-8183-021171d1aa23
# ╠═6c1f96fa-7810-4ed6-8c5e-5bbbedf1ab12
# ╠═d4d2ed12-391a-4ac3-a700-27a1868c5c54
# ╠═1352f986-bb42-43cd-97c6-8bebb0e2a747
# ╠═3286a4bc-63d7-477b-8ab8-a116beb3b9b7
# ╠═b1920d34-5be6-45c0-94ef-c097e54e23c7
# ╠═ef0da706-e06d-4c57-bef5-1e7b20310595
# ╠═c091e6b6-d022-4019-b006-0cdb5164a592
# ╠═9c39aba9-692b-48da-b0bb-423943b73dac
# ╠═003dceaa-48d3-4a00-a026-c1a43507c07e
# ╠═d37684ee-88a1-4b93-a6fb-9d91a28ac501
# ╠═de0f57a6-7f7d-461a-a40d-b1bf23b74c5f
# ╠═6fc17cbe-0db0-4407-b761-3c7cfaf7ec04
# ╠═8e4215d9-8975-47c6-9483-0c4c80de593b
# ╠═6e4ef700-47b1-47e8-b3d1-1cc68ffc9f96
# ╠═41553424-f87f-49d4-a708-d5dfb73c9216
# ╠═a525a083-6dd2-4e8d-a494-d3b9173ce935
# ╠═9f07b374-bdd3-41ae-a380-41d8cab1e8a5
# ╠═57166142-f792-4549-8570-784cc9fcb1fa
