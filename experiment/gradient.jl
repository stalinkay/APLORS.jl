### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 5e66d5d4-f931-11eb-1bb5-77572e0bcdc4
begin
	using Pkg; Pkg.activate(".."); Pkg.precompile()
	using Flux
	using DataFrames
	using Flux: chunk, crossentropy, @epochs, mse, onehotbatch, train!, update!
	using Zygote
	using PrettyTables
	using MLDataUtils
end

# ╔═╡ 05e7f077-db74-4540-ba46-511ced236657
model = Chain(Dense(2, 2, relu), Dense(2, 2, relu), Dense(2, 1))

# ╔═╡ 79886f3e-988b-48ca-94b6-2792ac568b78
X = randn(Float32, 2, 10_000)

# ╔═╡ 462d85f2-1c4a-4bb9-a13a-94640ee9959a
x = reshape(chunk(collect(X), 5)[2], (2, :))

# ╔═╡ 868ce896-fa8e-4476-a395-34c87fe38c5b
partitions(X::AbstractVecOrMat, n::Integer) = splitobs(X, [1/n for i in 1:n-1] |> Tuple)

# ╔═╡ bf5d7f75-2f0c-4758-bfee-28b5cc6cb38a
partitions(X, 5)

# ╔═╡ 9bb16bbe-2ecc-429e-844e-cebc863a6b21
partitions(X, 5)

# ╔═╡ c4d5d526-c6bf-4161-aab8-4a4a5615aa2c
sort([1/5 for i in 1:5-1], rev = false) |> Tuple

# ╔═╡ 30ecb1ec-e872-4c07-966c-59394216f0bc
y = round.(Int8, sum(X.^2, dims = 1)); y = abs.(y) #onehotbatch(y, sort(unique(y)))

# ╔═╡ f5773ae4-f21f-4e3e-b181-6564977ccc23
data = Flux.Data.DataLoader((X, y), batchsize = 32)

# ╔═╡ f5dcccbb-f743-4d3d-ad8a-0b18e6a807c3
ps = params(model)

# ╔═╡ 7d19bf8a-fa20-4547-ab3b-32b678e1c4b3
opt = NADAM()

# ╔═╡ 3b3f970f-36b4-4db2-8643-6b2e14366c5b
loss(x, y) = sqrt(mse(model(x), y))

# ╔═╡ b6e254d4-7e7d-4e6d-b0ce-0f3b3aa0cf26
accuracy(x, y) = sum(round.(Int8, model(x)) .== y) / size(y, 2)

# ╔═╡ 3c2db823-add8-4792-93f8-63ea8068d63a
loss(X, y)

# ╔═╡ 9292c6f3-3c90-4c04-be7b-baae2d7eb34a
accuracy(X, y)

# ╔═╡ 22de4564-613e-4bda-8472-d6e31dfade28
function custom_train!(loss, ps, data, opt)
  # training_loss is declared local so it will be available for logging outside the gradient calculation.
  local training_loss
  ps = Params(ps)
  for d in data
    gs = gradient(ps) do
      training_loss = loss(d...)
      # Code inserted here will be differentiated, unless you need that gradient information
      # it is better to do the work outside this block.
      return training_loss
    end
    # Insert whatever code you want here that needs training_loss, e.g. logging.
    # logging_callback(training_loss)
    # Insert what ever code you want here that needs gradient.
    # E.g. logging with TensorBoardLogger.jl as histogram so you can see if it is becoming huge.
		
		# weights_before = params(model)[1]
		# pretty_table(collect(params(model))[1])
		# local_gs = collect(gs)
		# global_gs = []
		# for lg in local_gs
		# 	gg = lg
		# 	append!(global_gs, gg)
		# end
		# gs = global_gs
			
    update!(opt, ps, gs)
		
		# weight_after = params(model)[1]
		# pretty_table(weight_after)
		# pretty_table(collect(gs))
    # Here you might like to check validation set accuracy, and break out to do early stopping.
  end
end

# ╔═╡ c6692d7c-8ffd-4f5d-bca8-4274ad431cdc
@epochs 200 custom_train!(loss, ps, data, opt)

# ╔═╡ bbfb8689-e096-4205-bf2a-11cdf230a118
loss(X, y)

# ╔═╡ 1fb41645-5858-45c2-b1a0-42b439837363
accuracy(X, y)

# ╔═╡ 697bd110-e866-48d7-8999-ba0177244ad0
for index in enumerate(1:length(Params(ps)))
	println(index)
end

# ╔═╡ b9ca77d0-a319-4b59-b36e-b44e2ab64a30
[(p1 - p2) for (p1, p2) in zip(collect(ps), collect(ps))]

# ╔═╡ e18f628b-6fcd-4a00-a9f8-76c842909588
collect(ps)

# ╔═╡ 2ccd955b-319c-4cb0-bddf-34eded834879
round.(Int8, model(X))

# ╔═╡ Cell order:
# ╠═5e66d5d4-f931-11eb-1bb5-77572e0bcdc4
# ╠═05e7f077-db74-4540-ba46-511ced236657
# ╠═79886f3e-988b-48ca-94b6-2792ac568b78
# ╠═462d85f2-1c4a-4bb9-a13a-94640ee9959a
# ╠═868ce896-fa8e-4476-a395-34c87fe38c5b
# ╠═bf5d7f75-2f0c-4758-bfee-28b5cc6cb38a
# ╠═9bb16bbe-2ecc-429e-844e-cebc863a6b21
# ╠═c4d5d526-c6bf-4161-aab8-4a4a5615aa2c
# ╠═30ecb1ec-e872-4c07-966c-59394216f0bc
# ╠═f5773ae4-f21f-4e3e-b181-6564977ccc23
# ╠═f5dcccbb-f743-4d3d-ad8a-0b18e6a807c3
# ╠═7d19bf8a-fa20-4547-ab3b-32b678e1c4b3
# ╠═3b3f970f-36b4-4db2-8643-6b2e14366c5b
# ╠═b6e254d4-7e7d-4e6d-b0ce-0f3b3aa0cf26
# ╠═3c2db823-add8-4792-93f8-63ea8068d63a
# ╠═9292c6f3-3c90-4c04-be7b-baae2d7eb34a
# ╠═22de4564-613e-4bda-8472-d6e31dfade28
# ╠═c6692d7c-8ffd-4f5d-bca8-4274ad431cdc
# ╠═bbfb8689-e096-4205-bf2a-11cdf230a118
# ╠═1fb41645-5858-45c2-b1a0-42b439837363
# ╠═697bd110-e866-48d7-8999-ba0177244ad0
# ╠═b9ca77d0-a319-4b59-b36e-b44e2ab64a30
# ╠═e18f628b-6fcd-4a00-a9f8-76c842909588
# ╠═2ccd955b-319c-4cb0-bddf-34eded834879
