WORK_DIR = haskey(ENV, "WORK_DIR") ? ENV["WORK_DIR"] : ".."

begin
	using Revise
	using Pkg
	Pkg.activate(WORK_DIR)
	# Pkg.precompile()
	using APLORS
	using BSON: @save
	using CSV: write
	using DataFrames
	using Dates: now
	using Flux: ADAM, Chain, Descent, Dense, Dropout, ADAM, NADAM, activations, chunk, crossentropy, flatten, kaiming_normal, label_smoothing, loadparams!, logitcrossentropy, mse, onecold, onehotbatch, params,  relu, sigmoid, softmax, throttle, train!, update!, @epochs, @functor
	using Flux.Data: DataLoader
	using MLDataUtils
    using MPI
	# using ONNXNaiveNASflux: save
	using PrettyTables: pretty_table
	using Printf: @printf
	using Random
	using Statistics: mean
	using Zygote: Params, gradient
	# using Tracker
end

MPI.Init()

comm = MPI.COMM_WORLD
current_rank = MPI.Comm_rank(comm)
comm_size = MPI.Comm_size(comm)
root = 0

# Environment
MODEL_NAME = haskey(ENV, "APLORS_MODEL_NAME") ? ENV["APLORS_MODEL_NAME"] : "nmf" # gmf, nmf, ncf
DROPOUT_RATE = haskey(ENV, "APLORS_DROPOUT_RATE") ? parse(Float32, ENV["APLORS_DROPOUT_RATE"]) : 0.5 # use dropout to reduce overfitting, should be coupled with a preceding batchnorm
USE_NONNEGATIVE_EMBEDDINGS = haskey(ENV, "APLORS_USE_NONNEGATIVE_EMBEDDINGS") ? parse(Bool, ENV["APLORS_USE_NONNEGATIVE_EMBEDDINGS"]) : parse(Bool, "1") # 0 or 1
USE_BIAS = haskey(ENV, "APLORS_USE_BIAS") ? parse(Bool, ENV["APLORS_USE_BIAS"]) : parse(Bool, "1") # 0 or 1
USE_CLASSIFICATION = haskey(ENV, "APLORS_USE_CLASSIFICATION") ? parse(Bool, ENV["APLORS_USE_CLASSIFICATION"]) : parse(Bool, "1") # 0 or 1
EMBEDDING_SIZE = haskey(ENV, "APLORS_EMBEDDING_SIZE") ? parse(Int, ENV["APLORS_EMBEDDING_SIZE"]) : 96 # a high value implies higher non-linearity

LEARNING_RATE = haskey(ENV, "APLORS_LEARNING_RATE") ? parse(Float32, ENV["APLORS_LEARNING_RATE"]) : 0.001 # smaller learning rate is better although it might take longer to descent to minima
OPTIMIZER = haskey(ENV, "APLORS_OPTIMIZER") ? ENV["APLORS_OPTIMIZER"] : "adam" # adam, descent, nadam
TOTAL_SAMPLES = haskey(ENV, "APLORS_TOTAL_SAMPLES") ? parse(Int, ENV["APLORS_TOTAL_SAMPLES"]) : 1_000
BATCH_SIZE = haskey(ENV, "APLORS_BATCH_SIZE") ? parse(Int, ENV["APLORS_BATCH_SIZE"]) : 64 # use a smaller batch size, converges faster
USE_STRATIFIED_OBS = haskey(ENV, "APLORS_USE_STRATIFIED_OBS") ? parse(Bool, ENV["APLORS_USE_STRATIFIED_OBS"]) : parse(Bool, "1") # 0 or 1
EPOCHS = haskey(ENV, "APLORS_EPOCHS") ? parse(Int, ENV["APLORS_EPOCHS"]) : 200

RANDOM_SEED = haskey(ENV, "APLORS_RANDOM_SEED") ? parse(Int, ENV["APLORS_RANDOM_SEED"]) : 7
CLUSTER = haskey(ENV, "APLORS_CLUSTER") ? ENV["APLORS_CLUSTER"] : "beowulf"

Random.seed!(RANDOM_SEED) # set global RNG for MLDataUtils

INDIR = joinpath(@__DIR__, MODEL_NAME)

# print("rank $(current_rank)\n")

EXPERIMENT_NAME = string(MODEL_NAME, "_", string(CLUSTER), "_cluster_", string(OPTIMIZER), "_optimizer_", string(LEARNING_RATE), "_learning_rate_", string(TOTAL_SAMPLES), "_ratings_", string(EMBEDDING_SIZE), "_embedding_size_", string(BATCH_SIZE), "_batch_size_", (USE_CLASSIFICATION ? "using_multi_classification_" : "using_binary_classification_"), (USE_STRATIFIED_OBS ? "using_stratified_obs_" : "without_stratified_obs_"), (USE_BIAS ? "using_bias_" : "without_bias_"), (USE_NONNEGATIVE_EMBEDDINGS ? "using_nonnegative_embeddings_" : "without_nonnegative_embeddings_"), EPOCHS, "_epochs_", RANDOM_SEED, "_random_seed")

if current_rank == root
	print(EXPERIMENT_NAME, "\n")
end

function prepare_mf_classification_data(df::DataFrame, at::Float64 = 0.9, use_stratified::Bool = false, use_classification_labels::Bool = false, rng::APLORS.DataPattern.AbstractRNGOrNothing = nothing)
	# if at == 1.0
	# 	# return no test
	# end
	X = Matrix(select(df, [:student, :learning_object]))' |> Matrix
	X = Float32.(X)
	
	y = use_classification_labels ? df[!, :rating] : Matrix(select(df, :rating))' |> Matrix
	y = use_classification_labels ? onehotbatch(y, 1:6) : y
	y = Int8.(y)
	
	data = (X, y)
	if use_stratified
		train, test = isnothing(rng) ? stratifiedobs(data, p = at, obsdim =  :last) : stratifiedobs(shuffleobs(data, obsdim = :last, rng = rng), p = at)
		return train, test
	else
		train, test = isnothing(rng) ? splitobs(data, at = at, obsdim =  :last) : splitobs(shuffleobs(data, obsdim = :last, rng = rng), at = at)
		return train, test
	end
	# splitobs(shuffleobs(data), 0.9)
	
	# X_train, y_train = train
	# X_test, y_test = test
	# X_train, y_train, X_test, y_test
end

if current_rank == root
	print("Current directory: ", @__DIR__, "\n")
	print("WORK_DIR directory from Environment: ", WORK_DIR, "\n")
	if haskey(ENV, "PBS_O_WORKDIR")
		print("PBS_O_WORKDIR: ", ENV["PBS_O_WORKDIR"], "\n")
	end
    print("Running on $(comm_size) processes\n")
end

# MPI.Barrier(comm)

if current_rank == root
	current_dir = joinpath(@__DIR__, "")
	dataset_path = joinpath(@__DIR__, "release-candidate/studentLearningObjectRatings.csv")

    training_df, validation_df, n_students, n_learning_objects = APLORS.DataPattern.mf_data(dataset_path, Random.seed!(RANDOM_SEED), TOTAL_SAMPLES, 0.1, 7, true)
	# print("training$(nrow(training_df))\n")
	# print(unique(sort(training_df[:, :rating])), "\n")
    train, test = prepare_mf_classification_data(training_df, 0.9, USE_STRATIFIED_OBS, USE_CLASSIFICATION)
	# print("$(size(train[1], 2))\n")
	_, validation_test = prepare_mf_classification_data(validation_df, 0.9, USE_STRATIFIED_OBS, USE_CLASSIFICATION)
else
    n_students = nothing
    n_learning_objects = nothing
    train = zeros(Float32, (2, 1))
    test = zeros(Float32, (2, 1))
	validation_test = zeros(Float32, (1, 1))
end

n_students = MPI.bcast(n_students, root, comm)
n_learning_objects = MPI.bcast(n_learning_objects, root, comm)

train = MPI.bcast(train, root, comm)
test = MPI.bcast(test, root, comm)

# validation_train = MPI.bcast(validation_train, root, comm)
validation_test = MPI.bcast(validation_test, root, comm)

n_procs = comm_size


X_train, y_train = train
X_test, y_test = test

# partitions(X::AbstractVecOrMat, n::Integer) = [reshape(chunk(X, n)[i], size(X, 1), :) for i in 1:n]
partitions(X::AbstractVecOrMat, n::Integer) = n > 1 ? splitobs(X, [1/n for i in 1:n-1] |> Tuple) : [X]

X_train = partitions(X_train, n_procs)[current_rank + 1]
y_train = USE_CLASSIFICATION ? onehotbatch(partitions(onecold(y_train), n_procs)[current_rank + 1], 1:6) : partitions(y_train, n_procs)[current_rank + 1]

X_test = partitions(X_test, n_procs)[current_rank + 1]
y_test = USE_CLASSIFICATION ? onehotbatch(partitions(onecold(y_test), n_procs)[current_rank + 1], 1:6) : partitions(y_test, n_procs)[current_rank + 1]

training_samples = (X_train, y_train)
testing_samples = (X_test, y_test)

if current_rank == root
	print("rank $current_rank: training samples: $(size(X_train, 2)), testing samples: $(size(X_test, 2)), validation samples: $(size(validation_test[1], 2)), total samples: $(size(train[1], 2) + size(test[1], 2))\n")

	print("training data on rank $(current_rank)\n")
	pretty_table(training_samples[1], USE_CLASSIFICATION ? onecold(training_samples[2]) : training_samples[2])

	print("testing data on rank $(current_rank)\n")
	pretty_table(testing_samples[1], USE_CLASSIFICATION ? onecold(testing_samples[2]) : testing_samples[2])
end

GMFBaseModel = APLORS.Models.GMF(n_students, n_learning_objects, EMBEDDING_SIZE, USE_NONNEGATIVE_EMBEDDINGS, false, kaiming_normal, Random.seed!(RANDOM_SEED))
NMFBaseModel = APLORS.Models.NMF(n_students, n_learning_objects, EMBEDDING_SIZE, USE_NONNEGATIVE_EMBEDDINGS, USE_BIAS, kaiming_normal, Random.seed!(RANDOM_SEED))
MLP = Chain(Dense(EMBEDDING_SIZE * 2, EMBEDDING_SIZE, relu), Dropout(DROPOUT_RATE), Dense(EMBEDDING_SIZE, EMBEDDING_SIZE, relu), Dropout(DROPOUT_RATE), Dense(EMBEDDING_SIZE, EMBEDDING_SIZE, relu), Dropout(DROPOUT_RATE), Dense(EMBEDDING_SIZE, EMBEDDING_SIZE, relu), Dropout(DROPOUT_RATE))
NCFBaseModel = APLORS.Models.NCF(GMFBaseModel, Chain(NMFBaseModel, MLP))

# custom_vcat(x, y) = vcat(x, y)
# @functor custom_vcat

GMFModel = Chain(GMFBaseModel, x -> USE_CLASSIFICATION ? Dense(EMBEDDING_SIZE, USE_CLASSIFICATION ? 6 : 1)(x) : sum(x, dims=1)) |> APLORS.Models.gpu_or_cpu
NMFModel = Chain(NMFBaseModel, MLP, Dense(EMBEDDING_SIZE, USE_CLASSIFICATION ? 6 : 1)) |> APLORS.Models.gpu_or_cpu
NCFModel = Chain(NCFBaseModel, Dense(EMBEDDING_SIZE * 2, USE_CLASSIFICATION ? 6 : 1)) |> APLORS.Models.gpu_or_cpu

model = GMFModel
model = MODEL_NAME == "nmf" ? NMFModel : model
model = MODEL_NAME == "ncf" ? NCFModel : model

if current_rank == root
	weights = params(model)
else
	weights = params(model)
end

weights = MPI.bcast(weights, root, comm)

loss_binary(x, y) = sqrt(mse(model(x), y))
loss_classification(x, y) = logitcrossentropy(model(x), Float32.(y))

function accuracy_binary(x, y)
	ŷ = Int8.(round.(model(x)))
	n = length(y)
	
	correct = sum(ŷ .== y)
	# if correct > n
	# 	pretty_table(Int8.(round.(model(x))))

	# 	# print("correct ", sum(model(x) .≈ y), "\n", typeof(sum(model(x) .≈ y)))
	# 	# print("\n\nŷsum = $(ŷ), n = $n\n\n")
		
	# 	print("correct: ", correct, "\ty total: ", n, "\tacc: ", correct/n, "\tŷ total", length(ŷ), "\n")
	# 	print("$(size(ŷ)) $(size(y))\n")
	# end
	acc = correct/n
	acc
end

accuracy_classification(x, y) = mean(onecold(model(x)) .== onecold(y))

loss = USE_CLASSIFICATION ? loss_classification : loss_binary

accuracy = USE_CLASSIFICATION ? accuracy_classification : accuracy_binary

ps = params(model)

data = DataLoader(training_samples, batchsize = BATCH_SIZE) |> APLORS.Models.gpu_or_cpu

η = LEARNING_RATE # Learning Rate
opt = OPTIMIZER == "adam" ? ADAM(η) : Descent(η)
opt = OPTIMIZER == "descent" ? opt : opt
opt = OPTIMIZER == "nadam" ? NADAM(η) : opt

function custom_train!(loss, ps, data, opt)
	# training_loss is declared local so it will be available for logging outside the gradient calculation.
	local training_loss
	ps = Params(ps)
	for d in data
		gs = gradient(ps) do
			training_loss = loss(d...)
			# Code inserted here will be differentiated, unless you need that gradient information
			# it is better to do the work outside this block.
			return training_loss
		end
		# Insert whatever code you want here that needs training_loss, e.g. logging.
		# logging_callback(training_loss)
		# Insert what ever code you want here that needs gradient.
		# E.g. logging with TensorBoardLogger.jl as histogram so you can see if it is becoming huge.
		update!(opt, ps, gs)
		# Here you might like to check validation set accuracy, and break out to do early stopping.
	end
end

n_testing_samples = round(Integer, size(testing_samples[1], 2)/10)

train_data = training_samples !== nothing ? training_samples : randobs((training_samples[1], training_samples[2]), n_testing_samples)
test_data = testing_samples !== nothing ? testing_samples : randobs((testing_samples[1], testing_samples[2]), n_testing_samples)
validation_data = validation_test !== nothing ? validation_test : randobs((validation_test[1], validation_test[2]), n_testing_samples)

function custom_train_mpi!(loss, ps, data, opt, EPOCHS)
	# training_loss is declared local so it will be available for logging outside the gradient calculation.
	local training_loss
	ps = Params(ps)

	batches = length(data)

	err_temp = hcat(loss(train_data[1], train_data[2]), loss(test_data[1], test_data[2]))
	err = hcat(err_temp, loss(validation_data[1], validation_data[2]))

	acc_temp = hcat(accuracy(train_data[1], train_data[2]), accuracy(test_data[1], test_data[2]))
	acc = hcat(acc_temp, accuracy(validation_data[1], validation_data[2]))

	for epoch in 1:EPOCHS
		for (index, d) in enumerate(data)
			gs = gradient(ps) do
				training_loss = loss(d...)
				# Code inserted here will be differentiated, unless you need that gradient information
				# it is better to do the work outside this block.
				return training_loss
			end
			# Insert whatever code you want here that needs training_loss, e.g. logging.
			# logging_callback(training_loss)
			# Insert what ever code you want here that needs gradient.
			# E.g. logging with TensorBoardLogger.jl as histogram so you can see if it is becoming huge.
			wts_before = params(model)
			update!(opt, ps, gs)
			wts_after = params(model)
	
			
			local_grads = [(wb - wa) for (wb, wa) in zip(collect(wts_before), collect(wts_after))]
			global_grads = []
			new_wts = []
	
			for (lg, wb) in zip(local_grads, wts_before)
				gg = similar(typeof(lg), axes(lg))
				MPI.Allreduce!(lg, gg, MPI.SUM, comm)
				gg /= n_procs
				append!(global_grads, gg)
				wt = wb - gg
				append!(new_wts, wt)
			end
	
			loadparams!(model, params(new_wts))
	
			# Here you might like to check validation set accuracy, and break out to do early stopping.
			# if current_rank == root
			#    @printf "l1 norm wts (sanity check): before = %.3f, after = %.3f\n" sum(abs.(wts_before[1])) sum(abs.(new_wts[1]))
			# #    pretty_table(wts_before[1])
			# #    pretty_table(new_wts)
			# end

			n_testing_samples = round(Integer, size(testing_samples[1], 2)/10)
			test_data = randobs((testing_samples[1], testing_samples[2]), n_testing_samples)

			local_loss, local_acc = (loss(testing_samples[1], testing_samples[2]), accuracy(testing_samples[1], testing_samples[2]))
			global_loss = 0
			global_acc = 0
			locals_ = [local_loss, local_acc]
			globals_ = zeros(2)
			MPI.Allreduce!(locals_, globals_, MPI.SUM, comm)

			# print("local accuracy: $(loss(testing_samples[1], testing_samples[2]))\t\tglobal accuracy: $(accuracy(testing_samples[1], testing_samples[2]))\n")

			global_loss = globals_[1]/n_procs
			global_acc = globals_[2]/n_procs
	
			if current_rank == root
				@printf "Epoch: %d \tMinibatchIndex: %d/%d \t\tloss: %.3f \t\tacc: %.2f\t\tn_testing_samples: %d\n" epoch index batches global_loss global_acc n_testing_samples
			end
		end

		err_temp = hcat(loss(train_data[1], train_data[2]), loss(test_data[1], test_data[2]))
		err_temp = hcat(err_temp, loss(validation_data[1], validation_data[2]))
		global err = vcat(err, err_temp)

		acc_temp = hcat(accuracy(train_data[1], train_data[2]), accuracy(test_data[1], test_data[2]))
		acc_temp = hcat(acc_temp, accuracy(validation_data[1], validation_data[2]))
		global acc = vcat(acc, acc_temp)
	end

	indir = joinpath(@__DIR__, "nmf")
	# Save loss and accuracy to csv for visualization
	write(joinpath(indir, "error.csv"), DataFrame(err, [:training, :testing, :validation]))
	write(joinpath(indir, "accuracy.csv"), DataFrame(acc, [:training, :testing, :validation]))
end

function callback()
	if current_rank == root
		err_temp, acc_temp = compute_training_metrics(train_data, test_data, validation_data)
		print("training loss: ", err_temp[1], "\t\ttest loss: ", err_temp[2], "\t\tvalidation loss: ", err_temp[3], "\n")
		print("training accuracy: ", acc_temp[1], "\t\ttest accuracy: ", acc_temp[2], "\t\tvalidation accuracy: ", acc_temp[3], "\n")
	end
end

function compute_training_metrics(train_eval_data::Tuple, test_eval_data::Tuple, validation_eval_data::Tuple)
	train_X, train_y = train_eval_data
	test_X, test_y = test_eval_data
	validation_X, validation_y = validation_eval_data

	local_loss_train, local_loss_test, local_loss_validation, local_accuracy_train, local_accuracy_test, local_accuracy_validation = (loss(train_X, train_y), loss(test_X, test_y), loss(validation_X, validation_y), accuracy(train_X, train_y), accuracy(test_X, test_y), accuracy(validation_X, validation_y))

	# global_loss_train = 0
	# global_loss_test = 0
	# global_loss_validation = 0

	# global_accuracy_train = 0
	# global_accuracy_test = 0
	# global_accuracy_validation = 0

	# locals_ = [local_loss_train, local_loss_test, local_loss_validation, local_accuracy_train, local_accuracy_test, local_accuracy_validation]
	# globals_ = similar(typeof(locals_), axes(locals_))
	# # globals_ = zeros(6)

	# MPI.Allreduce!(locals_, globals_, MPI.SUM, comm)

	# global_loss_train = globals_[1]/n_procs
	# global_loss_test = globals_[2]/n_procs
	# global_loss_validation = globals_[3]/n_procs

	# global_accuracy_train = globals_[4]/n_procs
	# global_accuracy_test = globals_[5]/n_procs
	# global_accuracy_validation = globals_[6]/n_procs


	# ([global_loss_train, global_loss_test, global_loss_validation]' |> Matrix, [global_accuracy_train, global_accuracy_test, global_accuracy_validation]' |> Matrix)
	([local_loss_train, local_loss_test, local_loss_validation]' |> Matrix, [local_accuracy_train, local_accuracy_test, local_accuracy_validation]' |> Matrix)

end

function custom_train_mpi2!(loss, ps, data, opt, EPOCHS)
	# training_loss is declared local so it will be available for logging outside the gradient calculation.

	ps = Params(ps)

	local err
	local acc

	if current_rank == root
		# print("Model params: ", ps[length(ps)], "\n")
		err, acc = compute_training_metrics(train_data, test_data, validation_data)
		# print("err: ", err, "\tacc: ", acc)
	end

	for epoch in 1:EPOCHS
		if current_rank == root
			print("Epoch: ", epoch, "\n")
			callback()
		end
		ps = params(model)

		wts_before = params(model)
		train!(loss, ps, data, opt)
		wts_after = params(model)
		
		local_grads = [(wb - wa) for (wb, wa) in zip(collect(wts_before), collect(wts_after))]
		global_grads = []
		new_wts = []

		for (lg, wb) in zip(local_grads, wts_before)
			gg = similar(typeof(lg), axes(lg))
			MPI.Allreduce!(lg, gg, MPI.SUM, comm)
			# print("loop err: ", err, "\tloop acc: ", acc)
			gg /= n_procs
			append!(global_grads, gg)
			wt = wb - gg
			append!(new_wts, wt)
		end

		loadparams!(model, params(new_wts))

		# if current_rank == root
		# 	@printf "Epoch: %d \tMinibatchIndex: %d/%d \t\tloss: %.3f \t\tacc: %.2f\t\tn_testing_samples: %d\n" epoch index batches global_loss global_acc n_testing_samples
		# end
		if current_rank == root
			err_temp, acc_temp = compute_training_metrics(train_data, test_data, validation_data)
			err = vcat(err, err_temp)
			acc = vcat(acc, acc_temp)
		end
	end
	
	# Save loss and accuracy to csv for visualization
	if current_rank == root
		write(joinpath(INDIR, string(EXPERIMENT_NAME, "-error.csv")), DataFrame(err, [:training, :testing, :validation]))
		write(joinpath(INDIR, string(EXPERIMENT_NAME, "-accuracy.csv")), DataFrame(acc, [:training, :testing, :validation]))
	end
end

@time custom_train_mpi2!(loss, ps, data, opt, EPOCHS)

if current_rank == root
	print("after training\n")

	callback() # display metrics
	# EXPERIMENT_NAME = string(EXPERIMENT_NAME, "-", "$(now())")
	model_path = "$(joinpath(INDIR, EXPERIMENT_NAME))"
	@save "$model_path.bson" model
	weights = params(model)
	@save "$(model_path)_weights.bson" weights

	# Save model as model.onnx where inputshapes are tuples with sizes of input.
	# Input shapes can be omitted in which case an attempt to infer the shapes will be made. If supplied, one tuple with size as the dimensions of the corresponding input array (including batch dimension) is expected.
	# save("$model_path.onnx", model, (EMBEDDING_SIZE, missing), USE_CLASSIFICATION ? (6, missing) : (1, missing))

	pretty_table(USE_CLASSIFICATION ? onecold(model(test[1])[:, 1:20])' : model(test[1])[:, 1:20])
	pretty_table(USE_CLASSIFICATION ? onecold(test[2])' : test[2])
end


MPI.Finalize()