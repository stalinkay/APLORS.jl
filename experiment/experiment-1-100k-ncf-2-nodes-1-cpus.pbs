#!/bin/bash

#PBS -q workq
#PBS -l select=2:ncpus=1:mpiprocs=1:ompthreads=1:mem=30GB
#PBS -l walltime=96:00:00
### Send email on abort, begin and end
#PBS -m abe
### Specify mail recipient
#PBS -M unam.na@stalinkay.work

# See: https://learn.scientificprogramming.io/learn-to-use-pbs-pro-job-scheduler-ffd9c0ad680d
# See: https://docs.computecanada.ca/wiki/Julia/en
# See: http://www.lpsm.paris/pageperso/sangnier/pbspython.html

export APLORS_MODEL_NAME=gmf # gmf, nmf, ncfparallel or ncf
export APLORS_DROPOUT_RATE=0.2 # use dropout to reduce overfitting, should be coupled with a preceding batchnorm
export APLORS_USE_NONNEGATIVE_EMBEDDINGS=0 # 0 or 1
export APLORS_USE_BIAS=1 # 0 or 1
export APLORS_USE_CLASSIFICATION=1 # 0 or 1
export APLORS_EMBEDDING_SIZE=8 # a high value implies higher non-linearity
export APLORS_MLP_EXTRA_LAYERS=1 # use odd number
export APLORS_USE_BATCH_NORM=0 # 0 or 1

export APLORS_USE_L2=0 # Doesn't work well with nmf, use ncf to combine gmf and ncf; ncf converges faster than ncfparallel
export APLORS_LEARNING_RATE=0.0001 # smaller learning rate is better although it might take longer to descent to minima
export APLORS_OPTIMIZER=adam # adam, descent, nadam
export APLORS_TOTAL_SAMPLES=100000
export APLORS_BATCH_SIZE=16 # use a smaller batch size, converges faster
export APLORS_USE_STRATIFIED_OBS=0 # 0 or 1
export APLORS_EPOCHS=100
export APLORS_USE_GRADIENTS=0
export APLORS_TEST_SIZE=0.1

export APLORS_RANDOM_SEED=7
export APLORS_CLUSTER=hpc

source $HOME/.bashrc
export WORK_DIR=$HOME/APLORS

cd "$WORK_DIR/experiment"

export APLORS_MODEL_NAME=ncf
time mpiexecjl --project=$WORK_DIR --mca orte_base_help_aggregate 0 --mca btl_base_warn_component_unused 0 -machinefile $PBS_NODEFILE -np 2 $HOME/.local/bin/julia --project=$WORK_DIR mpi.jl
echo ""; echo ""; echo ""