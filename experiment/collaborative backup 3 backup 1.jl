### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ d06b386e-f3ea-4abe-afc4-8e8f4916e901


# ╔═╡ Cell order:
# ╠═d06b386e-f3ea-4abe-afc4-8e8f4916e901
### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 81198964-f503-11eb-129e-c7c3aa24943b
begin
	using Revise
	using Pkg; Pkg.activate("..")
	using APLORS
	using Random
	using MLDataUtils
	using DataFrames
	using Flux: ADAM, Chain, Dense, Dropout, NADAM, activations, crossentropy, flatten, kaiming_normal, label_smoothing, logitcrossentropy, mse, onecold, onehotbatch, params,  relu, sigmoid, softmax, train!, update!, @epochs
	using Flux.Data: DataLoader
	using Statistics: mean
	using Zygote: Params, gradient
end

# ╔═╡ 7f5d4776-7f60-4ee3-8445-7e9a3b0eb81d
training_df, validation_df, n_students, n_learning_objects = APLORS.DataPattern.mf_data("release-candidate/studentLearningObjectRatings.csv", Random.seed!(7), nothing, 0.1, 7, false)

# ╔═╡ 48270ad3-69d1-40fb-8067-4e3aa1f6b6ad
function prepare_mf_classification_data(df::DataFrame, at::Float64 = 0.9, use_stratified::Bool = false, rng::APLORS.DataPattern.AbstractRNGOrNothing = nothing)
	X = Matrix(select(df, [:student, :learning_object]))' |> Matrix
	X = Float32.(X)
	
	y = df[!, :rating]
	y = onehotbatch(y, 1:6)
	y = Int8.(y)
	
	data = (X, y)
	if use_stratified
		train, test = isnothing(rng) ? stratifiedobs(data, p = at, obsdim =  :last) : stratifiedobs(shuffleobs(data, obsdim = :last, rng = rng), p = at)
		return train, test
	else
		train, test = isnothing(rng) ? splitobs(data, at = at, obsdim =  :last) : splitobs(shuffleobs(data, obsdim = :last, rng = rng), at = at)
		return train, test
	end
	# splitobs(shuffleobs(data), 0.9)
	
	# X_train, y_train = train
	# X_test, y_test = test
	# X_train, y_train, X_test, y_test
end

# ╔═╡ 0c30f144-e828-43a7-b34a-dc5971287e95
train, test = prepare_mf_classification_data(training_df)

# ╔═╡ 23dcbfcb-90c0-4353-b7b5-34f34bf7acfb
GMFModel = Chain(APLORS.Models.GMF(n_students, n_learning_objects, 1, true, true, kaiming_normal, Random.seed!(7)), softmax) |> APLORS.Models.gpu_or_cpu

# ╔═╡ a086223f-6959-4743-b324-0bfd8ccdce02
NMFModel = Chain(APLORS.Models.NMF(n_students, n_learning_objects, 24, false, true, kaiming_normal, Random.seed!(7)), Dense(48, 48, relu), Dropout(0.5), Dense(48, 32, relu), Dropout(0.5), Dense(32, 6), softmax) |> APLORS.Models.gpu_or_cpu

# ╔═╡ b213603d-a80a-4eed-9075-e0908c4aa0d6
model = NMFModel

# ╔═╡ ebfd29df-52ce-4945-bed8-1f65c1a1a0c6
sqnorm(x) = sum(abs2, x)

# ╔═╡ b0bee1ea-8af1-4900-aea4-fd863162aab9
penalty() = sum(abs2, model.weights)

# ╔═╡ 94d40d36-c71f-41d7-827c-81ce4d0917bc
# loss(x, y) = sqrt(mse(model(x), y))
loss(x, y) = crossentropy(model(x), Float32.(y))

# ╔═╡ 12c9ff28-aa7a-48da-a33f-39f899d38882
# accuracy(ŷ, y) = sum(ŷ .== y) / size(y, 2)
accuracy(ŷ, y) = mean(onecold(ŷ) .== onecold(y))

# ╔═╡ c58ca1ce-c99f-42e3-896f-ecfcd4b1e7c5
ps = params(model)

# ╔═╡ 2ed5e4f6-5972-4a78-aa50-740b5ac49bc9
data = DataLoader(train, batchsize = 32) |> APLORS.Models.gpu_or_cpu

# ╔═╡ d96f4729-3b07-4b4e-a773-afcac6430cb5
opt = NADAM()

# ╔═╡ 8712ec11-6209-48b8-a134-d87841ab93e1
println("Test loss = ", loss(test[1], test[2]))

# ╔═╡ 00785c26-2323-4efe-ac1f-6cab67c99196
function custom_train!(loss, ps, data, opt)
  # training_loss is declared local so it will be available for logging outside the gradient calculation.
  local training_loss
  ps = Params(ps)
  for d in data
    gs = gradient(ps) do
      training_loss = loss(d...)
      # Code inserted here will be differentiated, unless you need that gradient information
      # it is better to do the work outside this block.
      return training_loss
    end
    # Insert whatever code you want here that needs training_loss, e.g. logging.
    # logging_callback(training_loss)
    # Insert what ever code you want here that needs gradient.
    # E.g. logging with TensorBoardLogger.jl as histogram so you can see if it is becoming huge.
    update!(opt, ps, gs)
    # Here you might like to check validation set accuracy, and break out to do early stopping.
  end
end

# ╔═╡ 15666380-d08b-40cf-b397-36f40159a4ba
@time @epochs 10 custom_train!(loss, ps, data, opt)

# ╔═╡ 8510b1e4-6141-4460-926a-7d7587cbae15
println("After Test loss = ", loss(test[1], test[2]))

# ╔═╡ 84a2d0cc-5a99-4309-884d-00f11fc3dfd9
model(test[1])

# ╔═╡ dcc1d8a9-0d9f-4c4e-b310-24dab368446c
# onehotbatch(train[2], 1:6)

# ╔═╡ a3a8dd08-6a64-4f87-99f2-5db232165482
println("Train accuracy = ", accuracy(round.(Int8, model(train[1])), train[2]) * 100)

# ╔═╡ dba29ef0-2489-4ded-9ade-8776bde4d1ea
println("Test accuracy = ", accuracy(round.(Int8, model(test[1])), test[2]) * 100)

# ╔═╡ 837d8eb2-5142-4ec1-94e3-5ae0ff62b051
onecold(model(test[1])[:, 1:20])

# ╔═╡ 9925638e-e7c1-4cec-923c-b0932004f1ff
onecold(test[2][:, 1:20])

# ╔═╡ cf97b926-4578-47ae-8c2d-9bd6b8622050
test[2]

# ╔═╡ 21c80ba1-1c76-4071-9e24-5923df53f2f7
onecold([-1, -2, -2, -3, -3, 5])

# ╔═╡ 747ba7ab-a433-4436-836c-0865a73ff2c1
sum(sqnorm, params(model))

# ╔═╡ 840cae9b-4154-4eaa-8b0e-810221274540
sum(sqnorm, activations(model, train[1]))


# dumping this from mpi.jl

# function custom_train_mpi2!(loss, ps, data, opt, epochs)
# 	# training_loss is declared local so it will be available for logging outside the gradient calculation.
# 	local training_loss
# 	ps = Params(ps)

# 	for epoch in 1:epochs
# 		for (index, d) in enumerate(data)
# 			gs = gradient(ps) do
# 				training_loss = loss(d...)
# 				# Code inserted here will be differentiated, unless you need that gradient information
# 				# it is better to do the work outside this block.
# 				return training_loss
# 			end
# 			# Insert whatever code you want here that needs training_loss, e.g. logging.
# 			# logging_callback(training_loss)
# 			# Insert what ever code you want here that needs gradient.
# 			# E.g. logging with TensorBoardLogger.jl as histogram so you can see if it is becoming huge.

# 			wts_before = collect(params(model))

# 			update!(opt, ps, gs)

# 			wts_after = collect(params(model))

# 			local_wts = [(wb - wa) for (wb, wa) in zip(collect(wts_before), collect(wts_after))]
# 			global_wts = []
# 			new_wts = []

# 			for (lg, wb) in (local_wts, wts_before)
# 				gg = similar(typeof(lg), axes(lg))
# 				MPI.Allreduce!(lg, gg, MPI.SUM, comm)
# 				gg /= n_procs
# 				append!(global_wts, gg)
# 				wt = wb - gg
# 				append!(new_wts, wt)
# 			end

# 			global_wts /= n_procs
# 			loadparams!(model, params(new_wts))

# 			# Here you might like to check validation set accuracy, and break out to do early stopping.
# 			if current_rank == root
# 			   @printf "l1 norm wts (sanity check): before = %.3f, after = %.3f\n" sum(abs.(wts_before[1])) sum(abs.(new_wts[1]))
# 			   print("before = $(wts_before[1])\n\n, after = $(new_wts[1])\n\n")
# 			end

# 			local_loss, local_acc = (loss(testing_samples[1], testing_samples[2]), accuracy(testing_samples[1], testing_samples[2]))
# 			global_loss = 0
# 			global_acc = 0
# 			locals_ = [local_loss, local_acc]
# 			globals_ = zeros(2)
# 			MPI.Allreduce!(locals_, globals_, MPI.SUM, comm)
# 			global_loss = globals_[1]/n_procs
# 			global_acc = globals_[2]/n_procs
	
# 			if current_rank == root
# 				@printf "Epoch: %d  MinibatchIndex: %d/%d loss: %.3f acc: %.2f\n" epoch index n_minibatches global_loss global_acc
# 			end
# 		end
# 	end
# end

# function custom_train_mpi3!(loss, ps, data, opt, epochs)
# 	# training_loss is declared local so it will be available for logging outside the gradient calculation.
# 	local training_loss
# 	ps = Params(ps)

# 	for epoch in 1:epochs
# 		for (index, d) in enumerate(data)
# 			gs = gradient(ps) do
# 				training_loss = loss(d...)
# 				# Code inserted here will be differentiated, unless you need that gradient information
# 				# it is better to do the work outside this block.
# 				return training_loss
# 			end
# 			# Insert whatever code you want here that needs training_loss, e.g. logging.
# 			# logging_callback(training_loss)
# 			# Insert what ever code you want here that needs gradient.
# 			# E.g. logging with TensorBoardLogger.jl as histogram so you can see if it is becoming huge.

# 			local_gs = collect(gs)
# 			global_gs = []
# 			for lg in local_gs
# 				gg = similar(typeof(lg), axes(lg))
# 				MPI.Allreduce!(lg, gg, MPI.SUM, comm)
# 				gg /= n_procs
# 				append!(global_gs, gg)
# 			end
# 			gs = global_gs

# 			wts_before = collect(params(ps))
# 			update!(opt, ps, gs)
# 			wts_after = collect(params(ps))

# 			# Here you might like to check validation set accuracy, and break out to do early stopping.
# 			if current_rank == root
# 			   @printf "l1 norm wts (sanity check): before = %.3f, after = %.3f\n" sum(abs.(wts_before[1])) sum(abs.(wts_after[1]))
# 			   print("before = $(wts_before[1])\n\n, after = $(wts_after[1])\n\n")
# 			end

# 			local_loss, local_acc = (loss(testing_samples[1], testing_samples[2]), accuracy(testing_samples[1], testing_samples[2]))
# 			global_loss = 0
# 			global_acc = 0
# 			locals_ = [local_loss, local_acc]
# 			globals_ = zeros(2)
# 			MPI.Allreduce!(locals_, globals_, MPI.SUM, comm)
# 			global_loss = globals_[1]/n_procs
# 			global_acc = globals_[2]/n_procs
	
# 			if current_rank == root
# 				@printf "Epoch: %d  MinibatchIndex: %d/%d loss: %.3f acc: %.2f\n" epoch index n_minibatches global_loss global_acc
# 			end
# 		end
# 	end
# end