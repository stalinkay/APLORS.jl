### A Pluto.jl notebook ###
# v0.14.6

using Markdown
using InteractiveUtils

# ╔═╡ 6a88d48c-9286-43e9-8aa1-ca2d71f4a716
using Revise

# ╔═╡ b9ea8492-add1-11eb-0a99-31e01d53e6b6
import APLORS.Layers: Embedding

# ╔═╡ 4698ca10-0c38-413f-a3c0-f5307e9aadc7
import APLORS.Models: GMF, NMF

# ╔═╡ ec594a76-d82a-4b24-9926-e6adecd2ef84
import Flux: kaiming_normal, glorot_normal, onehotbatch, flatten, params, ADAM, mae, mse, Chain, identity, σ, Dense, softmax, sigmoid, cpu, Zeros

# ╔═╡ 48201a68-e614-45c6-9675-4c04022c9e00
import LinearAlgebra: dot

# ╔═╡ b2b911aa-0365-4db2-907b-bf31dcffe20f
import Statistics: mean, mean!

# ╔═╡ 7a493898-0265-423f-8299-16ac3e0564e6
import Random

# ╔═╡ 16acc120-776c-4acc-a79c-755320545fcf
in = 2

# ╔═╡ 37ce4ca3-5f1b-4afb-ba5a-93a252170cea
out = 3

# ╔═╡ bf851704-0e9f-44e2-8979-9682bd516fc6
embedding_size = out

# ╔═╡ 765d39b6-58a1-4b49-8f72-31585a914cff
vocab_size = in

# ╔═╡ dd9728a7-3689-4e00-8de6-15d5f5157135
begin
    Random.seed!(7)
    embedding1 = Embedding(in, out, false)
    embedding2 = Embedding(in, out, true)
    embedding3 = Embedding(in, out, false, false, glorot_normal)
    embedding4 = Embedding(in, out, true, false, glorot_normal)
	embedding1, embedding2, embedding3, embedding4, embedding4, flatten(embedding1.bias)
end

# ╔═╡ b42efcbe-c680-47f6-b6bf-4c47102086bb
embedding1([1, 2, 1, 1])

# ╔═╡ 9309e359-9dc6-425e-ab3d-07595a84eda0
import APLORS; names(APLORS.Models); GMF(Embedding(1000, 50, true, true, kaiming_normal), Embedding(2000, 50))([1 2; 1 3])

# ╔═╡ 8df1899f-9292-406a-a2f0-8a57e0291236
Random.seed!(7); NMF(Embedding(1000, 50, true), Embedding(2000, 50, true))([1 1; 2 2]), Chain(NMF(1000, 2000, 50))([1 1; 2 2])

# ╔═╡ 27a70a2d-2062-4289-8818-a9b3cfcdc6d2
Random.seed!(7); model = Chain(GMF(1000, 2000, 50))([1 1; 2 2])

# ╔═╡ 8c481160-5a29-48b5-ba0d-e346942500f3
opt = ADAM()

# ╔═╡ b653273b-7c6b-492f-b3ae-c0e11d634c7b
loss(x, y) = mse(model(x), y)

# ╔═╡ 497d739f-9405-44ac-91df-8ba0845f2f52
f(x, y = 1, z = missing) = ismissing(z)

# ╔═╡ d1417cd6-e36d-4731-8d0e-4e97c02b9805
f(1)

# ╔═╡ 40d6173b-2845-4157-a6b4-8b8e63b88162
embedding1, identity(embedding1.weight), σ.(embedding1.weight), softmax(embedding1.weight), softmax(embedding1.weight), sigmoid == σ

# ╔═╡ d2e224a8-7cc3-437c-aadf-7b15a673134e
Random.seed!(7); embedding = Dense(7, 3, identity).weight, Embedding(7, 3).weight

# ╔═╡ c00b97a6-2929-458e-86d8-f77647963a6d
mse([1, 3], [2, 5]), mae([1, 3], [2, 5]), sqrt(mse([1, 3], [2, 5]))

# ╔═╡ 2930ee54-dc0c-457a-9555-0c85243c4cf7
dense = Dense(3, 2); dense.weight, dense.bias

# ╔═╡ ba8b2838-38ac-4ed8-a2fe-a647471393fd
randn(2, 3), randn(2)

# ╔═╡ Cell order:
# ╠═6a88d48c-9286-43e9-8aa1-ca2d71f4a716
# ╠═b9ea8492-add1-11eb-0a99-31e01d53e6b6
# ╠═4698ca10-0c38-413f-a3c0-f5307e9aadc7
# ╠═ec594a76-d82a-4b24-9926-e6adecd2ef84
# ╠═48201a68-e614-45c6-9675-4c04022c9e00
# ╠═b2b911aa-0365-4db2-907b-bf31dcffe20f
# ╠═7a493898-0265-423f-8299-16ac3e0564e6
# ╠═16acc120-776c-4acc-a79c-755320545fcf
# ╠═37ce4ca3-5f1b-4afb-ba5a-93a252170cea
# ╠═bf851704-0e9f-44e2-8979-9682bd516fc6
# ╠═765d39b6-58a1-4b49-8f72-31585a914cff
# ╠═dd9728a7-3689-4e00-8de6-15d5f5157135
# ╠═b42efcbe-c680-47f6-b6bf-4c47102086bb
# ╠═9309e359-9dc6-425e-ab3d-07595a84eda0
# ╠═8df1899f-9292-406a-a2f0-8a57e0291236
# ╠═27a70a2d-2062-4289-8818-a9b3cfcdc6d2
# ╠═8c481160-5a29-48b5-ba0d-e346942500f3
# ╠═b653273b-7c6b-492f-b3ae-c0e11d634c7b
# ╠═497d739f-9405-44ac-91df-8ba0845f2f52
# ╠═d1417cd6-e36d-4731-8d0e-4e97c02b9805
# ╠═40d6173b-2845-4157-a6b4-8b8e63b88162
# ╠═d2e224a8-7cc3-437c-aadf-7b15a673134e
# ╠═c00b97a6-2929-458e-86d8-f77647963a6d
# ╠═2930ee54-dc0c-457a-9555-0c85243c4cf7
# ╠═ba8b2838-38ac-4ed8-a2fe-a647471393fd
