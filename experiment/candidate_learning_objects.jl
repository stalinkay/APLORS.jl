### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ e05fe5a6-4d8e-4e66-8879-953dedcb52c0
begin
	# using Revise
	using Pkg; Pkg.activate("..")
	using APLORS
	using PrettyTables
end

# ╔═╡ 1ad0e32b-6004-4c92-abfb-c66613442776
using DataFrames: DataFrame, Not, describe, groupby, innerjoin, nrow, order, reduce, select, vcat

# ╔═╡ fc84692d-f6ec-4dda-b6f7-fe5c049d4804
using Random

# ╔═╡ d99f8fb0-e30f-43ff-88e8-ebbfd6157719
import Statistics: mean

# ╔═╡ 8774be9e-6a3f-4ee2-9231-1dd0e73d3598
import TextAnalysis: tf_idf

# ╔═╡ 2c80fc83-3f8e-44bf-8df4-cf85193181f7
# begin
# 	using Plots
# 	plot(
# 	    plot(X[1,:], zcolor=L, m=2, t=:scatter3d, leg=false, title="Swiss Roll"),
# 		leg=false)
# end

# ╔═╡ bf5be5b6-82bd-43e7-b2aa-b6560adaeade
md"""
## Prepare Learning Objects
"""

# ╔═╡ 11cf95ac-67b5-4abe-a346-c92f84c6ef03
lo_df = APLORS.DataPattern.read_data_frame("release-candidate/learningObjects.csv") |> DataFrame

# ╔═╡ cb286c3b-dd84-4a16-80e3-f529b8809e25
c_df = APLORS.DataPattern.read_data_frame("release-candidate/courses.csv") |> DataFrame

# ╔═╡ 640a25d5-5060-4cb5-8f2b-f069d6ab1f8e
cp_df = APLORS.DataPattern.read_data_frame("release-candidate/coursePrerequisites.csv") |> DataFrame

# ╔═╡ 9203efc1-0ce4-4637-a7ae-5d505e6d8ab2
t_df = APLORS.DataPattern.read_data_frame("release-candidate/topics.csv") |> DataFrame

# ╔═╡ be5e1bb5-1f84-487f-ac45-968d7a8ed809
tp_df = APLORS.DataPattern.read_data_frame("release-candidate/topicPrerequisites.csv") |> DataFrame

# ╔═╡ fb2db354-aebb-4be3-b7f9-888d6ea08cd2
learning_object_df = APLORS.DataPattern.prepare_learning_objects(lo_df, c_df, cp_df, t_df, tp_df, 20000, Random.seed!(7))

# ╔═╡ d0f60346-1f77-4a62-93eb-b94c413eee93
query = "FFF2014J url"

# ╔═╡ 8b897f69-16b1-4d30-a670-a86d2785498d
APLORS.DataPattern.find_candidate_learning_objects_by_query(query, learning_object_df, 200, true, APLORS.DataPattern.cosine, APLORS.DataPattern.tf_algo)

# ╔═╡ 1c0f1d94-7f32-4c24-9830-5595b0658a17
md"""
## Prepare Students
"""

# ╔═╡ 6de96ea3-b435-473b-b5a9-628541177bfe
s_df = APLORS.DataPattern.read_data_frame("release-candidate/students.csv")

# ╔═╡ b2f5c84e-deb1-44ef-8543-0d5ae5bbd33f
spt_df = APLORS.DataPattern.read_data_frame("release-candidate/studentPreferredTopics.csv")

# ╔═╡ 3f927f48-02be-4fba-8b2d-9da9d0cec888
sce_df = APLORS.DataPattern.read_data_frame("release-candidate/studentCourseEnrollments.csv")

# ╔═╡ ec208848-860c-4d16-a766-27ea41500258
student_df = APLORS.DataPattern.prepare_students(s_df, spt_df, sce_df, c_df, cp_df, nothing, Random.GLOBAL_RNG)

# ╔═╡ 636caea5-1e8d-47bb-8180-44162d4eaab8
names(APLORS.DataPattern)

# ╔═╡ c2bf098c-45ba-486d-a725-ec2081810cbf
md"""
## Combine Learning Objects with Students
"""

# ╔═╡ 864ce398-785e-4a8e-9aef-bd87fc20cf1a
lo_candidate_df = APLORS.DataPattern.prepare_student_learning_objects(learning_object_df, student_df)

# ╔═╡ 2c35304d-0700-4d1e-aaf0-99d48925aeb2
l, s = APLORS.DataPattern.prepare_content_data(lo_candidate_df, student_df, learning_object_df, [:description, :cosine_distance, :lo_label])

# ╔═╡ 5339c48c-ac99-4fd4-ad38-e75b02d89da9
slor_df, _, _ = APLORS.DataPattern.load_dataset("release-candidate/studentLearningObjectRatings.csv"); slor_df = APLORS.DataPattern.add_bias(slor_df)

# ╔═╡ f35bbaf1-8efc-499c-9b53-6c817d9f71e9
ll, ss = APLORS.DataPattern.prepare_learning_objects_for_student(613387, s, l, slor_df)

# ╔═╡ e67758a5-298f-4679-a896-bc19bb400ece
cbf1_df = APLORS.CBF.recommend_with_dr(select(ss, Not([:learning_object_id, :student_id]))[1, :] |> DataFrame, select(ll, Not([:learning_object_id, :student_id])), 10000, APLORS.DataPattern.correlation, true, 100, 100, false); cbf1_df = select(cbf1_df, [:learning_object, :rating_predicted, :rating]); cbf1_df = innerjoin(cbf1_df, select(l, [:learning_object, :learning_object_id]), on = [:learning_object]); cbf1_df = innerjoin(cbf1_df, lo_candidate_df[lo_candidate_df.learning_object_id .!== missing, :], on = [:learning_object, :learning_object_id]); cbf1_df = sort(cbf1_df, [order(:rating_predicted, rev = false)])

# ╔═╡ ea55ca49-a655-4bc6-9115-cf7a78fae2ef
cbf_rec_df = APLORS.CBF.recommend(select(ss, Not([:learning_object_id, :student_id]))[1, :] |> DataFrame, select(ll, Not([:learning_object_id, :student_id])) |> DataFrame, 10000, APLORS.DataPattern.euclidean, true); cbf_rec_df = select(cbf_rec_df, [:learning_object, :rating_predicted, :rating]); cbf_rec_df = innerjoin(cbf_rec_df, select(l, [:learning_object, :learning_object_id]), on = [:learning_object]); cbf_rec_df = innerjoin(cbf_rec_df, lo_candidate_df[lo_candidate_df.learning_object_id .!== missing, :], on = [:learning_object, :learning_object_id]); cbf_rec_df = sort(cbf_rec_df, [order(:rating_predicted, rev = false)])

# ╔═╡ fed2ba92-b0f7-4d34-b93a-f58aedb75352
# cbf_nl_rec_df = APLORS.CBF.recommend_with_dr(select(ss, Not([:learning_object_id, :student_id]))[1, :] |> DataFrame, select(ll, Not([:learning_object_id, :student_id])), 10000, APLORS.DataPattern.correlation, true, 100, 100, true); cbf_nl_rec_df = select(cbf_nl_rec_df, [:learning_object, :rating_predicted, :rating]); cbf_nl_rec_df = innerjoin(cbf_nl_rec_df, select(l, [:learning_object, :learning_object_id]), on = [:learning_object]); innerjoin(cbf_nl_rec_df, lo_candidate_df[lo_candidate_df.learning_object_id .!== missing, :], on = [:learning_object, :learning_object_id])

# ╔═╡ c35785b5-74db-4604-8f95-06bc1e7109a9
student_df[student_df.student_id .== 613387, :][1:end, :] |> DataFrame

# ╔═╡ e1312d85-e08c-44c0-bce9-6f629ca0d694
lo_candidate_df

# ╔═╡ a6af6b6e-a1d5-48b5-a988-244ab5311185
a = DataFrame(a = 1:5)

# ╔═╡ 2bdb6c5c-6d0e-4e17-ba3c-bd9a729d5bf9
b = DataFrame(b = 6:10)

# ╔═╡ 2beef7c1-86fd-4018-8d38-179f455a87a0
reduce(vcat, [a, b], cols = :union, source = :source)

# ╔═╡ 70b0dcce-92dd-4e56-9e80-65e1d3a502c3
X_df = reduce(vcat, [select(ll, Not([:learning_object_id, :student_id]))[1:10, :] |> DataFrame, select(ss, Not([:learning_object_id, :student_id]))[1, :] |> DataFrame], cols = :union, source = nothing); Xte = Matrix(X_df)'

# ╔═╡ 81b050e0-8418-4653-92bb-6b90dab7db5a
begin
	using ManifoldLearning
	using NearestNeighbors
	
	X, L = ManifoldLearning.swiss_roll()
	
	X
	
	M = fit(Isomap, Xte, nntype = APLORS.CBF.KDTree; k = 10, maxoutdim = 10)
	
	Y = transform(M)
	
	
	tf_idf(Y' |> Matrix)'
	
	#println("M = $(M)")
	
	#println("Y = $(size(Y)), X = $(size(X))")
end

# ╔═╡ Cell order:
# ╠═e05fe5a6-4d8e-4e66-8879-953dedcb52c0
# ╠═1ad0e32b-6004-4c92-abfb-c66613442776
# ╠═fc84692d-f6ec-4dda-b6f7-fe5c049d4804
# ╠═d99f8fb0-e30f-43ff-88e8-ebbfd6157719
# ╠═8774be9e-6a3f-4ee2-9231-1dd0e73d3598
# ╠═2c80fc83-3f8e-44bf-8df4-cf85193181f7
# ╟─bf5be5b6-82bd-43e7-b2aa-b6560adaeade
# ╠═11cf95ac-67b5-4abe-a346-c92f84c6ef03
# ╠═cb286c3b-dd84-4a16-80e3-f529b8809e25
# ╠═640a25d5-5060-4cb5-8f2b-f069d6ab1f8e
# ╠═9203efc1-0ce4-4637-a7ae-5d505e6d8ab2
# ╠═be5e1bb5-1f84-487f-ac45-968d7a8ed809
# ╠═fb2db354-aebb-4be3-b7f9-888d6ea08cd2
# ╠═d0f60346-1f77-4a62-93eb-b94c413eee93
# ╠═8b897f69-16b1-4d30-a670-a86d2785498d
# ╟─1c0f1d94-7f32-4c24-9830-5595b0658a17
# ╠═6de96ea3-b435-473b-b5a9-628541177bfe
# ╠═b2f5c84e-deb1-44ef-8543-0d5ae5bbd33f
# ╠═3f927f48-02be-4fba-8b2d-9da9d0cec888
# ╠═ec208848-860c-4d16-a766-27ea41500258
# ╠═636caea5-1e8d-47bb-8180-44162d4eaab8
# ╟─c2bf098c-45ba-486d-a725-ec2081810cbf
# ╠═864ce398-785e-4a8e-9aef-bd87fc20cf1a
# ╠═2c35304d-0700-4d1e-aaf0-99d48925aeb2
# ╠═5339c48c-ac99-4fd4-ad38-e75b02d89da9
# ╠═f35bbaf1-8efc-499c-9b53-6c817d9f71e9
# ╠═e67758a5-298f-4679-a896-bc19bb400ece
# ╠═ea55ca49-a655-4bc6-9115-cf7a78fae2ef
# ╠═fed2ba92-b0f7-4d34-b93a-f58aedb75352
# ╠═c35785b5-74db-4604-8f95-06bc1e7109a9
# ╠═e1312d85-e08c-44c0-bce9-6f629ca0d694
# ╠═a6af6b6e-a1d5-48b5-a988-244ab5311185
# ╠═2bdb6c5c-6d0e-4e17-ba3c-bd9a729d5bf9
# ╠═2beef7c1-86fd-4018-8d38-179f455a87a0
# ╠═70b0dcce-92dd-4e56-9e80-65e1d3a502c3
# ╠═81b050e0-8418-4653-92bb-6b90dab7db5a
