### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 924ed724-bdbc-4c84-8eef-bbecb5a795bb
using Random

# ╔═╡ 15e2dc8b-abce-4a26-8d07-530d83e15b45
using Flux: ADAM, mse, train!, params, @epochs, throttle, @show, params, softmax

# ╔═╡ f9fb7236-1a9e-41e5-8e52-2561d7d1e753
using Tracker

# ╔═╡ 8e96fc19-0cf0-43f2-bf55-40e02ee2c3ec
using Flux.Data: DataLoader

# ╔═╡ 058a9d82-d7f0-4ded-b82f-22d6fdd640f4
using Flux: @functor, glorot_normal, kaiming_normal

# ╔═╡ 96773aef-1a62-48b2-96bf-b691638538ba
using Flux: onehotbatch, onehot

# ╔═╡ 78f0e4e5-1cdd-4a11-8e94-2d062734ecba
using DataFrames

# ╔═╡ 539d410b-6423-450e-b621-fa4a1b83a37e
using CSV: File, write

# ╔═╡ bb9383b7-67a3-4a90-aaf6-f78361c93c74
using PrettyTables: pretty_table

# ╔═╡ 0ed3c027-fd11-4b9e-9afa-4e2017de97d9
using ScikitLearn

# ╔═╡ c7a28f83-7429-43b6-9c21-fa1eed8d6f3a
include("../src/model.jl")

# ╔═╡ 569ae4f5-737f-431c-b020-21284db01ab4
@sk_import preprocessing:LabelEncoder

# ╔═╡ 2b106aab-ec0e-4ba8-8b2f-d8cb2d8995e8
import ScikitLearn.CrossValidation: train_test_split

# ╔═╡ 798fd302-a4fc-4879-96b9-96e07d68a1c3
import Statistics: mean

# ╔═╡ 2671e255-bcc0-4c9b-add7-1b0afd29cae8
Random.seed!(RANDOM_SEED)

# ╔═╡ 5c07c449-c3ff-4f16-927e-40a5e24631b9
WORK_DIR = pwd()

# ╔═╡ df1934c5-0894-4271-9670-c7d8cd0e0d21
DATASET_PATH = "$WORK_DIR/release-candidate/"

# ╔═╡ 6a943ad8-67e4-4435-b7c3-0583c4eb9d0b
read_data_frame(x) = File(x) |> DataFrame!

# ╔═╡ 169b8a45-a3d6-47b0-9a6b-2d9eccd37187
@time begin
    println("dataset loading started")
    Random.seed!(RANDOM_SEED)
    ratings = read_data_frame(string(DATASET_PATH, "studentLearningObjectRatings.csv"))
    # ratings = read_data_frame(string(DATASET_PATH, "ratings.csv"))
    # ratings = read_data_frame(string(DATASET_PATH, "BX-Book-Ratings.csv"))
    # ratings = read_data_frame(string(DATASET_PATH, "withdrawn_studentLearningObjectRatings_dummy.csv"))
    ratings = ratings[shuffle(1:size(ratings, 1)), :]
    ratings[!, :μ] .= mean(ratings[!, :rating])
    gdf = groupby(ratings, :student_id)
    means = combine(gdf, :rating => mean => :student_bias)
    ratings = innerjoin(ratings, means, on = :student_id)

    gdf = groupby(ratings, :learning_object_id)
    means = combine(gdf, :rating => mean => :learning_object_bias)
    ratings = innerjoin(ratings, means, on = :learning_object_id)
    @time ratings = first(ratings, 100_000)
    println("dataset loading completed")
end

# ╔═╡ 58f4daa3-013e-4905-b964-475a3695b0aa
# train, test = TrainTestSplit(df, 0.75)

# ╔═╡ 9459d49e-1bf6-4179-b37c-32e2edc710c8
student_enc = LabelEncoder()

# ╔═╡ 7329e4df-bf70-4840-ac94-bea251b9459a
ratings[!, :student] = fit_transform!(student_enc, ratings[!, :student_id])

# ╔═╡ 02d6de9b-a300-4a5d-b9a2-abdd19a0dbda
n_students = length(unique(ratings[!, :student]))

# ╔═╡ 887a149a-0ef0-4939-be3c-e5454b779b0a
learning_object_enc = LabelEncoder()

# ╔═╡ 07be306d-b5b4-4a4d-ae6c-de5ae13cb9da
ratings[!, :learning_object] =
    fit_transform!(learning_object_enc, ratings[!, :learning_object_id])

# ╔═╡ 201b7b8e-259c-4917-96de-e71e938a699b
n_learning_objects = length(unique(ratings[!, :learning_object]))

# ╔═╡ 3cdaa472-bb6e-4eb8-8eba-fc4952cc7aa1
min_rating = minimum(ratings[!, :rating])

# ╔═╡ 95f67427-fc7d-4735-b5d0-40ea02e707fe
max_rating = maximum(ratings[!, :rating])

# ╔═╡ b9d42fd4-a7b2-45a5-88b3-9d7a336bbcff
X = convert(Matrix, select(ratings, [:student, :learning_object]))

# ╔═╡ fa8f50e1-f18d-4da1-8e7d-f68522f36a11
y = convert(Matrix, select(ratings, [:rating]))

# ╔═╡ 0b31bcfd-343c-4464-a439-fbd3beed5514
X_train, X_test, y_train, y_test =
    train_test_split(X, y, test_size = 0.2, random_state = RANDOM_SEED)

# ╔═╡ d5bd305c-fdbd-41ad-bfa3-77f6a8949413
Random.seed!(RANDOM_SEED)

# ╔═╡ def2d92e-66de-4740-9b6f-77b0a6936376
X_test'

# ╔═╡ 60b39d6c-34c3-4eeb-a2d5-86b2570103af
MFRec(X_test')

# ╔═╡ 7ca019b0-2622-4137-b953-190668682338
loss(x, y) = mse(MFRec(x), y)

# ╔═╡ 1d5f4672-e284-4a60-adb7-ee8de34598cb
function accuracy(x, y)
    # println(model_v1(x))
    # println(size(model_v1(x)))
    # println(y)
    # println(size(y))

    # println("x==y: $(model_v1(x) .== y), y = $(size(y, 2)), y = $(size(y, 1)), sum correct = $(sum(model_v1(x) .== y)), argmax x = $(Flux.argmax(model_v1(x)))")

    return sum(MFRec(x) .== y) / size(y, 2)
end

# ╔═╡ f6414286-4386-4c01-a9a6-ac79cee0f964
begin
    struct Pro
        model::Any
    end

    (e::Pro)(x) = e.model(x)
    @functor Pro
end

# ╔═╡ 9f1ab73b-2193-4aa7-8c3c-5ce553a7a6ea
begin
    function print_data(x)
        # println("x = $(x), $(typeof(x)), $(size(x)), $(x[2, :]), $(size(flatten(x[2, :])))")
        x
    end

    p = Pro(
        Chain(
            print_data,
            x -> Dense(1, 1, relu)(x[1, :]') .* Dense(1, 1, relu)(x[2, :]'),
            Dense(1, 1),
        ),
    )
end

# ╔═╡ e355d6ea-9cc4-48b0-8b37-5d22f4b458a1
begin
    Random.seed!(RANDOM_SEED)
    struct Product2
        e1::Any
        e2::Any
    end

    function print_data2(x)
        println(x)
        x
    end

    (m::Product2)(x) = Chain(
        x1 -> dot(x1[1, :]),
        flatten(m.e2(x1[2, :])),
        x4 -> print_data2(x4),
        x3 -> Dense(size(x3, 1), 1)(x3),
    )(
        x,
    )
    @functor Product2

    Embed(vocab_size::Integer, max_features::Integer) =
        kaiming_normal(max_features, vocab_size)

    # 	struct Embeddings
    # 		W
    # 	end

    # 	(m::Embedding)(x) = reshape(m.W, 1, :) * x
    # 	@functor Embedding

    # reshape(Embedding(n_students, 3), 1, :) .* X_test'[1, :]

    student_embeddings = Embed(n_students, 5)
    learning_object_embeddings = Embed(n_learning_objects, 5)

    student_embeddings, learning_object_embeddings

    u = student_embeddings * onehotbatch(X_train'[1, :], 0:n_students-1)
    v = learning_object_embeddings * onehotbatch(X_train'[2, :], 0:n_learning_objects-1)

    # u = [1 3 5 1 0; 4 2 5 -4 0]
    # v = [3 1 5 -2 2; 2 1 3 -1 0]

    # u, v, dot.(u, v), dot.(u[:, ], v[:, ]), dot(u[:, 1], v[:, 1]), u' * v

    @time dot.(u, v), @time sum(dot.(u, v), dims = 1)
end

# ╔═╡ a876638c-a1ef-41bd-939a-39891ef83145
begin
    Random.seed!(RANDOM_SEED)
    struct Embeddings
        W::AbstractArray
        vocab_size::Integer
        nonnegative::Bool
    end
    Embeddings(vocab_size::Integer, max_features::Integer, nonnegative = true) =
        Embeddings(kaiming_normal(max_features, vocab_size), vocab_size, nonnegative) # convenience method
    (m::Embeddings)(x) = abs.(m.W * onehotbatch(x .+ 1, 1:m.vocab_size)) # increment x
    @functor Embeddings
    mdl = Embeddings(n_students, 5)
    println("Test")
    mdl(X_train'[1, :])
    # minimum(X_train'[1, :])
end

# ╔═╡ 0a6f04a9-bfea-4ede-a7cb-b816e40305cd
ps = params(MFRec)

# ╔═╡ 7274f6ad-5a59-4119-ab18-1d0b1c2d396b
opt = ADAM()

# ╔═╡ aae7197a-d0c5-422e-9610-c5a5be35f169
data = DataLoader((X_train', y_train'), batchsize = 32)

# ╔═╡ 020f3453-cfaf-415e-9a52-a5c5e762d02f
begin
    for epoch = 1:2
        @time train!(loss2, params(MFRec3), data, ADAM())
        @show epoch, loss2(X_train', y_train'), loss2(X_test', y_test')
    end
end

# ╔═╡ 5d89f423-604c-4548-902c-4981447fc876
typeof(y_train')

# ╔═╡ 60bdd314-434f-4f29-bf32-845e4817f84b
begin
    student_embedding, learning_object_embedding = _create_embeddings(EMBEDDING_SIZE)
    Product(student_embedding, learning_object_embedding)([1 2; 1 2])
end

# ╔═╡ c084f3cb-637a-4015-ae0c-9fd57304a3fa
# train!(loss, ps, data, opt)

# ╔═╡ 2c026c39-18f7-41fa-bf9c-80e2888f7b6e
# flatten([1, 2, 3])'

# ╔═╡ 54ba3cb9-cf2b-474c-b1e6-a26d85d36182
# begin
# 	# later
# 	# @epochs 10 train!(loss, ps, data, opt, cb = cb)

# 	err = hcat(Tracker.data(loss(X_train', y_train')), Tracker.data(loss(X_test', y_test')))
# 	acc = hcat(accuracy(X_train', y_train'), accuracy(X_test', y_test'))
# 	for epoch = 1:10
# 		train!(loss, ps, data, opt)
# 		global err = vcat(err, hcat(Tracker.data(loss(X_train', y_train')), Tracker.data(loss(X_test', y_test'))))
# 	    global acc = vcat(acc, hcat(accuracy(X_train', y_train'), accuracy(X_test', y_test')))
# 		@show epoch, loss(X_train', y_train'), loss(X_test', y_test'), accuracy(X_train', y_train'), accuracy(X_test', y_test')
# 	end
# end

# ╔═╡ 5740b449-103a-463b-88ac-43d85800cebc
# p(X_test')

# ╔═╡ 90b1195f-2bc5-49c1-a664-0ad40bb61bac
"$(MFRec)"

# ╔═╡ 184354fe-47f9-4913-8062-2240b9c741e8
begin
    write("$WORK_DIR/mfrec/error-flux.csv", DataFrame(err, [:training, :testing]))
    write("$WORK_DIR/mfrec/accuracy-flux.csv", DataFrame(acc, [:training, :testing]))
end

# ╔═╡ 4cb17312-70e1-4e27-8f05-c216270bcc12
# params(MFRec)

# ╔═╡ 243df8b7-1e26-4a01-891b-17e9261ce9fc
# X_test'

# ╔═╡ 9b1d9899-ce31-440b-93c3-1a40a6c22bb8
# MFRec(X_test')

# ╔═╡ daf6abd6-5a37-4df2-b4e5-6d943fcfe2ba
# round.(Int, MFRec(X_test'))

# ╔═╡ Cell order:
# ╠═924ed724-bdbc-4c84-8eef-bbecb5a795bb
# ╠═15e2dc8b-abce-4a26-8d07-530d83e15b45
# ╠═f9fb7236-1a9e-41e5-8e52-2561d7d1e753
# ╠═8e96fc19-0cf0-43f2-bf55-40e02ee2c3ec
# ╠═058a9d82-d7f0-4ded-b82f-22d6fdd640f4
# ╠═96773aef-1a62-48b2-96bf-b691638538ba
# ╠═78f0e4e5-1cdd-4a11-8e94-2d062734ecba
# ╠═539d410b-6423-450e-b621-fa4a1b83a37e
# ╠═bb9383b7-67a3-4a90-aaf6-f78361c93c74
# ╠═0ed3c027-fd11-4b9e-9afa-4e2017de97d9
# ╠═569ae4f5-737f-431c-b020-21284db01ab4
# ╠═2b106aab-ec0e-4ba8-8b2f-d8cb2d8995e8
# ╠═798fd302-a4fc-4879-96b9-96e07d68a1c3
# ╠═c7a28f83-7429-43b6-9c21-fa1eed8d6f3a
# ╠═2671e255-bcc0-4c9b-add7-1b0afd29cae8
# ╠═5c07c449-c3ff-4f16-927e-40a5e24631b9
# ╠═df1934c5-0894-4271-9670-c7d8cd0e0d21
# ╠═6a943ad8-67e4-4435-b7c3-0583c4eb9d0b
# ╠═169b8a45-a3d6-47b0-9a6b-2d9eccd37187
# ╠═58f4daa3-013e-4905-b964-475a3695b0aa
# ╠═9459d49e-1bf6-4179-b37c-32e2edc710c8
# ╠═7329e4df-bf70-4840-ac94-bea251b9459a
# ╠═02d6de9b-a300-4a5d-b9a2-abdd19a0dbda
# ╠═887a149a-0ef0-4939-be3c-e5454b779b0a
# ╠═07be306d-b5b4-4a4d-ae6c-de5ae13cb9da
# ╠═201b7b8e-259c-4917-96de-e71e938a699b
# ╠═3cdaa472-bb6e-4eb8-8eba-fc4952cc7aa1
# ╠═95f67427-fc7d-4735-b5d0-40ea02e707fe
# ╠═b9d42fd4-a7b2-45a5-88b3-9d7a336bbcff
# ╠═fa8f50e1-f18d-4da1-8e7d-f68522f36a11
# ╠═0b31bcfd-343c-4464-a439-fbd3beed5514
# ╠═d5bd305c-fdbd-41ad-bfa3-77f6a8949413
# ╠═def2d92e-66de-4740-9b6f-77b0a6936376
# ╠═60b39d6c-34c3-4eeb-a2d5-86b2570103af
# ╠═7ca019b0-2622-4137-b953-190668682338
# ╠═1d5f4672-e284-4a60-adb7-ee8de34598cb
# ╠═f6414286-4386-4c01-a9a6-ac79cee0f964
# ╠═9f1ab73b-2193-4aa7-8c3c-5ce553a7a6ea
# ╠═e355d6ea-9cc4-48b0-8b37-5d22f4b458a1
# ╠═a876638c-a1ef-41bd-939a-39891ef83145
# ╠═0a6f04a9-bfea-4ede-a7cb-b816e40305cd
# ╠═7274f6ad-5a59-4119-ab18-1d0b1c2d396b
# ╠═aae7197a-d0c5-422e-9610-c5a5be35f169
# ╠═020f3453-cfaf-415e-9a52-a5c5e762d02f
# ╠═5d89f423-604c-4548-902c-4981447fc876
# ╠═60bdd314-434f-4f29-bf32-845e4817f84b
# ╠═c084f3cb-637a-4015-ae0c-9fd57304a3fa
# ╠═2c026c39-18f7-41fa-bf9c-80e2888f7b6e
# ╠═54ba3cb9-cf2b-474c-b1e6-a26d85d36182
# ╠═5740b449-103a-463b-88ac-43d85800cebc
# ╠═90b1195f-2bc5-49c1-a664-0ad40bb61bac
# ╠═184354fe-47f9-4913-8062-2240b9c741e8
# ╠═4cb17312-70e1-4e27-8f05-c216270bcc12
# ╠═243df8b7-1e26-4a01-891b-17e9261ce9fc
# ╠═9b1d9899-ce31-440b-93c3-1a40a6c22bb8
# ╠═daf6abd6-5a37-4df2-b4e5-6d943fcfe2ba
