### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ bfcfa66a-1a1c-4994-98dd-98e5cc01c994
using TextAnalysis, Clustering, NearestNeighbors, DataFrames, Distances, BenchmarkTools, LinearAlgebra, Flux, Random

# ╔═╡ 3f076ff0-ca33-11eb-2b26-63b85858318f
crps = Corpus([
		StringDocument("To be or not to be"),
		StringDocument("This must be the crazy"),
		StringDocument("To be a man or not to be a man"),
		StringDocument("This is crazy"),
		StringDocument("To become or not to become"),
		StringDocument("To or not to"),
		StringDocument("To be the woman or not to be the woman"),
		StringDocument("Crazy thing I like"),
		StringDocument("This is crazying"),
		StringDocument("To be it or not to be it"),
		StringDocument("This is crap"),
		StringDocument("Crazy")]); standardize!(crps, StringDocument);  [prepare!(sd, strip_punctuation | strip_articles) for sd in crps[1:12]]; stem!(crps); crps[5]

# ╔═╡ df200d3c-5a55-4dfe-8142-6108911e3cae
#crps = Corpus(crps[1:7]);

# ╔═╡ 41dbf3c2-7474-4f40-8389-31d5f6c500dd
remove_case!(crps)

# ╔═╡ 5ede3843-9af6-42ae-b63b-ac751ba9b07f
crps[1:12]

# ╔═╡ cc9dbb74-2866-4ab3-b392-cba2edf1f973
update_lexicon!(crps)

# ╔═╡ 949a214c-583d-4f3f-a12b-d362e841d5ad
update_inverse_index!(crps)

# ╔═╡ eb04992d-3166-4765-9f68-9754d4f78c5e
crps["to"]

# ╔═╡ 80cb738e-c617-445e-b27a-1939f5eace54
m = DocumentTermMatrix(crps)

# ╔═╡ d0c2ebf9-be43-43e2-abe4-0e7a10f6b70b
D = dtm(m, :dense)

# ╔═╡ 62814eb6-9183-497c-ab5c-5bceec4ba936
T = tf_idf(D)

# ╔═╡ 5e19caed-f34c-4974-bd97-290eea14810f
T'

# ╔═╡ 9bcd7086-2422-4af9-8ccc-d57f4aaf408e
cl = kmeans(T, 2)

# ╔═╡ 3a1c79b6-c571-4ad6-8381-4a1a72796d21
T'

# ╔═╡ 48d4aae4-42da-4e42-a36b-d3544195b978
k = 9; point = T'[:, 1]

# ╔═╡ 7022e3a1-2166-402c-a239-3046a0535311
tree1 = BruteTree(T'[:, 2:12], Euclidean())

# ╔═╡ 55dd8d2d-cde7-4f9e-8b87-6b3474d5fa93
names(NearestNeighbors)

# ╔═╡ 3f720c99-1344-4b40-a49c-2b78e680c5c7
idxs, dists = knn(tree1, point, k, true)

# ╔═╡ 6e66d9bc-6685-4040-a355-df0cf142d467
lsa_result = lsa(crps)

# ╔═╡ a97bbefd-2562-47eb-b4a4-e7a0c35368df
latent_space = lsa_result.U * Diagonal(lsa_result.S) * lsa_result.Vt

# ╔═╡ 80432e27-4de7-4b31-acef-88794d19a223
lsa_result.U * lsa_result.Vt

# ╔═╡ 94a8f02c-ef62-4714-aafa-a1df6b83a3f9
latent_space'[:, 1]

# ╔═╡ d3a757e8-a7cb-406e-a05f-6043349c2a31
tree2 = BallTree(latent_space'[:, 1:11], Hamming())

# ╔═╡ 66cfecb1-c05b-4ef7-abc9-85b3778084e0
point2 = latent_space'[:, 12]

# ╔═╡ dee80d9a-5ea6-44bb-8fc9-fca94919be6a
idxs2, dists2 = knn(tree2, point2, k, true)

# ╔═╡ ae694404-660f-4a70-b09e-b01359fdab29
latent_space, point

# ╔═╡ f225832c-46c2-47a0-9980-ce04ce190f20
iterations = 1000 # number of gibbs sampling iterations

# ╔═╡ 8dc910cd-6eaa-43c6-b047-12f9394953f2
α = 1.1      # hyper parameter

# ╔═╡ 861540b3-293c-4c5d-a7ba-a6142c9163cb
β  = 1.1       # hyper parameter

# ╔═╡ 25b33609-21f8-4cda-896f-cda799ceb1bf
ϕ, θ  = lda(m, 3, iterations, α, β)

# ╔═╡ c3f98503-ae45-4c41-8f7a-5e21af1d837b
newArray = θ

# ╔═╡ 2d7d55e6-4a09-453d-a20c-a3e16377670b
tree3 = BruteTree(newArray[:, 1:11], Hamming())

# ╔═╡ e2daef41-0e49-4dea-8c46-8832e1a13bbd
point3 = newArray[:, 12]

# ╔═╡ 8a3024ca-69fe-4c86-bb2b-66bb559a46a5
idxs3, dists3 = knn(tree3, point3, k, true)

# ╔═╡ b8ff588f-2834-4559-9f45-e81d24ab48e2
crps[idxs]

# ╔═╡ 25163eae-d477-4519-bcf5-c24ed7773fe7
crps[idxs3]

# ╔═╡ Cell order:
# ╠═bfcfa66a-1a1c-4994-98dd-98e5cc01c994
# ╠═3f076ff0-ca33-11eb-2b26-63b85858318f
# ╠═df200d3c-5a55-4dfe-8142-6108911e3cae
# ╠═41dbf3c2-7474-4f40-8389-31d5f6c500dd
# ╠═5ede3843-9af6-42ae-b63b-ac751ba9b07f
# ╠═cc9dbb74-2866-4ab3-b392-cba2edf1f973
# ╠═949a214c-583d-4f3f-a12b-d362e841d5ad
# ╠═eb04992d-3166-4765-9f68-9754d4f78c5e
# ╠═80cb738e-c617-445e-b27a-1939f5eace54
# ╠═d0c2ebf9-be43-43e2-abe4-0e7a10f6b70b
# ╠═62814eb6-9183-497c-ab5c-5bceec4ba936
# ╠═5e19caed-f34c-4974-bd97-290eea14810f
# ╠═9bcd7086-2422-4af9-8ccc-d57f4aaf408e
# ╠═3a1c79b6-c571-4ad6-8381-4a1a72796d21
# ╠═48d4aae4-42da-4e42-a36b-d3544195b978
# ╠═7022e3a1-2166-402c-a239-3046a0535311
# ╠═55dd8d2d-cde7-4f9e-8b87-6b3474d5fa93
# ╠═3f720c99-1344-4b40-a49c-2b78e680c5c7
# ╠═6e66d9bc-6685-4040-a355-df0cf142d467
# ╠═a97bbefd-2562-47eb-b4a4-e7a0c35368df
# ╠═80432e27-4de7-4b31-acef-88794d19a223
# ╠═94a8f02c-ef62-4714-aafa-a1df6b83a3f9
# ╠═d3a757e8-a7cb-406e-a05f-6043349c2a31
# ╠═66cfecb1-c05b-4ef7-abc9-85b3778084e0
# ╠═dee80d9a-5ea6-44bb-8fc9-fca94919be6a
# ╠═ae694404-660f-4a70-b09e-b01359fdab29
# ╠═f225832c-46c2-47a0-9980-ce04ce190f20
# ╠═8dc910cd-6eaa-43c6-b047-12f9394953f2
# ╠═861540b3-293c-4c5d-a7ba-a6142c9163cb
# ╠═25b33609-21f8-4cda-896f-cda799ceb1bf
# ╠═c3f98503-ae45-4c41-8f7a-5e21af1d837b
# ╠═2d7d55e6-4a09-453d-a20c-a3e16377670b
# ╠═e2daef41-0e49-4dea-8c46-8832e1a13bbd
# ╠═8a3024ca-69fe-4c86-bb2b-66bb559a46a5
# ╠═b8ff588f-2834-4559-9f45-e81d24ab48e2
# ╠═25163eae-d477-4519-bcf5-c24ed7773fe7
