### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 81198964-f503-11eb-129e-c7c3aa24943b
begin
	# using Revise
	using Pkg; Pkg.activate("..")
	using APLORS
	using Random
	using MLDataUtils
	using DataFrames
	using Flux: ADAM, Chain, crossentropy, kaiming_normal, onecold, onehotbatch, params, mse, softmax, train!, @epochs
	using Flux.Data: DataLoader
	using Statistics: mean
end

# ╔═╡ 7f5d4776-7f60-4ee3-8445-7e9a3b0eb81d
training_df, validation_df, n_students, n_learning_objects = APLORS.DataPattern.mf_data("release-candidate/studentLearningObjectRatings.csv", nothing, nothing, 0.1, 7, false)

# ╔═╡ 48270ad3-69d1-40fb-8067-4e3aa1f6b6ad
function prepare_mf_classification_data(df::DataFrame, at::Float64 = 0.9, use_kfolds::Bool = false, rng::APLORS.DataPattern.AbstractRNGOrNothing = nothing)
	X = Matrix(select(df, [:student, :learning_object]))' |> Matrix
	y = Matrix(select(df, [:rating]))' |> Matrix
	y = onehotbatch(y, 0:5)
	data = (X, y)
	train, test = isnothing(rng) ? splitobs(data, at = at, obsdim =  :last) : splitobs(shuffleobs(data, obsdim =  :last, rng = rng), at = at)
	# splitobs(shuffleobs(data), 0.9)
	
	# X_train, y_train = train
	# X_test, y_test = test
	# X_train, y_train, X_test, y_test
end

# ╔═╡ 0c30f144-e828-43a7-b34a-dc5971287e95
train, test = prepare_mf_classification_data(training_df)

# ╔═╡ 23dcbfcb-90c0-4353-b7b5-34f34bf7acfb
GMFModel = Chain(APLORS.Models.GMF(n_students, n_learning_objects, 6, true, true, kaiming_normal, nothing), softmax)

# ╔═╡ 94d40d36-c71f-41d7-827c-81ce4d0917bc
loss(x, y) = crossentropy(GMFModel(x), y)

# ╔═╡ 9473f6aa-1582-49a0-956c-0aa3c7f5f55f
# loss(train[1], train[2])

# ╔═╡ c58ca1ce-c99f-42e3-896f-ecfcd4b1e7c5
ps = params(GMFModel)

# ╔═╡ 2ed5e4f6-5972-4a78-aa50-740b5ac49bc9
data = DataLoader(train, batchsize = 32)

# ╔═╡ d96f4729-3b07-4b4e-a773-afcac6430cb5
opt = ADAM()

# ╔═╡ 15666380-d08b-40cf-b397-36f40159a4ba
@time @epochs 1 train!(loss, ps, data, opt)

# ╔═╡ 8510b1e4-6141-4460-926a-7d7587cbae15
loss(test[1], test[2])

# ╔═╡ 84a2d0cc-5a99-4309-884d-00f11fc3dfd9
GMFModel(test[1])

# ╔═╡ dcc1d8a9-0d9f-4c4e-b310-24dab368446c
onehotbatch(train[2], 0:5)

# ╔═╡ 12c9ff28-aa7a-48da-a33f-39f899d38882
accuracy(ŷ, y) = mean(onecold(ŷ) .== onecold(y))

# ╔═╡ 78f7730d-16dc-4ce4-a88b-a5ef33e90d42
println("accuracy = $(accuracy(GMFModel(test[1]), test[2]))")

# ╔═╡ Cell order:
# ╠═81198964-f503-11eb-129e-c7c3aa24943b
# ╠═7f5d4776-7f60-4ee3-8445-7e9a3b0eb81d
# ╠═48270ad3-69d1-40fb-8067-4e3aa1f6b6ad
# ╠═0c30f144-e828-43a7-b34a-dc5971287e95
# ╠═23dcbfcb-90c0-4353-b7b5-34f34bf7acfb
# ╠═94d40d36-c71f-41d7-827c-81ce4d0917bc
# ╠═9473f6aa-1582-49a0-956c-0aa3c7f5f55f
# ╠═c58ca1ce-c99f-42e3-896f-ecfcd4b1e7c5
# ╠═2ed5e4f6-5972-4a78-aa50-740b5ac49bc9
# ╠═d96f4729-3b07-4b4e-a773-afcac6430cb5
# ╠═15666380-d08b-40cf-b397-36f40159a4ba
# ╠═8510b1e4-6141-4460-926a-7d7587cbae15
# ╠═84a2d0cc-5a99-4309-884d-00f11fc3dfd9
# ╠═dcc1d8a9-0d9f-4c4e-b310-24dab368446c
# ╠═12c9ff28-aa7a-48da-a33f-39f899d38882
# ╠═78f7730d-16dc-4ce4-a88b-a5ef33e90d42
