### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 0e56c163-c59f-45c5-9b2e-5d0faa140b18
import Random

# ╔═╡ aa91e604-e08e-41f1-9be7-2d2ebf7d7f17
import Flux:
    Dense,
    Chain,
    cpu,
    relu,
    sigmoid,
    onehotbatch,
    flatten,
    Dropout,
    BatchNorm,
    params,
    @functor

# ╔═╡ 5480672b-95bc-430c-a58d-ec51454d1c2c
import LinearAlgebra: dot

# ╔═╡ 83980722-1cf8-4a08-bc63-f9f161ce6848
md"""
## Models
"""

# ╔═╡ 80e70c08-78cc-446e-b6bc-788c84149405
versioninfo(verbose = false)

# ╔═╡ 427ef58e-b6d7-4208-a503-4114cb6d173f
md"""
### 1. Prepare Environment

i.e. embedding size, autoencoder size and GPU or CPU
"""

# ╔═╡ 18165846-7922-4f71-9456-113882670229
EMBEDDING_SIZE =
    haskey(ENV, "APLORS_EMBEDDING_SIZE") ? parse(Int, ENV["APLORS_EMBEDDING_SIZE"]) : 50 # a high value implies higher non-linearity

# ╔═╡ c9937231-4aa9-401d-bacd-826edce74cae
AUTOENCODER_INPUT_SIZE =
    haskey(ENV, "APLORS_AUTOENCODER_INPUT_SIZE") ?
    parse(Int, ENV["APLORS_AUTOENCODER_INPUT_SIZE"]) : 3 # a high value implies higher non-linearity

# ╔═╡ 0671a158-1b71-4def-bee7-b10247255de8
RANDOM_SEED = haskey(ENV, "APLORS_RANDOM_SEED") ? parse(Int, ENV["APLORS_RANDOM_SEED"]) : 7

# ╔═╡ 203f5f37-b5c9-4c28-9a17-11e9e1247db0
Random.seed!(RANDOM_SEED)

# ╔═╡ 8a630bab-32d3-4f6b-af67-1b8221b117df
gpu_or_cpu = cpu

# ╔═╡ 61a88885-587b-4443-9ba3-367a9fd995e5
md"""
### 2. Specify Embedding Layer
"""

# ╔═╡ 6149bcff-f158-4a55-a832-b3b57c45ccd8
Embedding(input_size, output_size, activation) = Dense(input_size, output_size, activation)

# ╔═╡ eca69416-c4f2-4355-8d54-a8b28d2c5e4d
md"""
### 3. Model Definitions
"""

# ╔═╡ c551cfe1-d545-447a-8b03-8d64bd394d79
md"""
##### 3.1 Matrix Factorization Model
"""

# ╔═╡ 03ccfe70-3914-4d2e-81f2-9bbaf6d5c0bd
md"""
1. [Can flux.jl handle networks with more than one input? #452](https://github.com/FluxML/Flux.jl/issues/452)
2. [[Solved]Flux.jl: can it be used to train networks with multiple distinct inputs?](https://discourse.julialang.org/t/solved-flux-jl-can-it-be-used-to-train-networks-with-multiple-distinct-inputs/16498)
"""

# ╔═╡ 2e6a74db-caf2-45ec-8a25-dbf6cc7c6302
function _create_embeddings(embedding_size)
    student_input = Dense(1, 1)
    student_embedding_layer = Embedding(1, embedding_size, identity)
    student_embedding = Chain(student_input, student_embedding_layer, flatten)

    learning_object_input = Dense(1, 1)
    learning_object_embedding_layer = Embedding(1, embedding_size, identity)
    learning_object_embedding =
        Chain(learning_object_input, learning_object_embedding_layer, flatten)
    # println("student_size = $(size(student_embedding)), lo_size = $(size(learning_object_embedding))")
    student_embedding, learning_object_embedding
end

# ╔═╡ fa0eac79-1720-49b9-82ca-f6f2ffc06a10
md"""
Convert 
1. [How to train a combination of models in Flux?](https://stackoverflow.com/questions/61530621/how-to-train-a-combination-of-models-in-flux/61537182#61537182)
"""

# ╔═╡ 17efd88b-ff51-4674-914e-bf1dc5d14bb0
begin
    struct Product
        model1::Any
        model2::Any
    end

    (e::Product)(x) = Chain(
        x -> dot.(e.model1(flatten(x[1, :])), e.model2(flatten(x[2, :]))),
        Dense(50, 50, relu),
        Dense(50, 1),
    )(
        x,
    )
    @functor Product
end

# ╔═╡ 6df1d68c-039c-4d98-84c0-4124234e29c7
# function _create_MFRec(embedding_size)
#     student_embedding, learning_object_embedding = _create_embeddings(embedding_size)
#     product(S, L) = dot(student_embedding(S), learning_object_embedding(L)) |> gpu_or_cpu
# end

# ╔═╡ 0d2ada1f-d8fb-40eb-8b21-48083ff63901
function _create_MFRec(embedding_size)
    student_embedding, learning_object_embedding = _create_embeddings(embedding_size)
    Chain(Product(student_embedding, learning_object_embedding))
end

# ╔═╡ ec12425f-d267-4886-82e7-f193af0c8adb
begin
    # Random.seed!(RANDOM_SEED)
    MFRec = _create_MFRec(EMBEDDING_SIZE)
end

# ╔═╡ 211a2da1-cb99-425a-97d6-a2abe1a382ba
# MFRec([1, 2, 3]', [2,3,4]')

# ╔═╡ 4448c7bc-11c2-4ff5-a9ab-511f658b85ff
# length(abs.([[-0.2, -10]; [-0.2, -12]]))

# ╔═╡ a9a4eccd-3d8a-4a9b-a1eb-9cb8721db7c4
md"""
##### 3.2 Matrix Factorization Multi-Layer Perceptron Model
"""

# ╔═╡ 5dc59ffb-68fd-4b71-b8fc-2bde561ff90e
function _create_MFMLP(embedding_size, input_size = 1, use_batchnorm = true)
    if use_batchnorm
        Chain(
            Embedding(1, 1, identity),
            BatchNorm(1),
            Dropout(0.2),
            Dense(1, 1, relu),
            BatchNorm(1),
            Dropout(0.2),
            Dense(1, 1, relu),
            BatchNorm(1),
            Dropout(0.2),
            Dense(1, 1, sigmoid),
        ) |> gpu_or_cpu
    else
        Chain(
            Embedding(1, 1, identity),
            Dropout(0.2),
            Dense(1, 1, relu),
            Dropout(0.2),
            Dense(1, 1, relu),
            Dropout(0.2),
            Dense(1, 1, sigmoid),
        ) |> gpu_or_cpu
    end
end

# ╔═╡ d9a686dc-4acf-44e4-9186-35febf9b5246
function _create_MFMLPRec(embedding_size)
    Chain(x -> [MFRec(x[1], x[2])], flatten, x -> _create_MFMLP(embedding_size)) |>
    gpu_or_cpu
end

# ╔═╡ f05a2888-1a08-484d-9473-60dcca438236
MFMLPRec = _create_MFMLPRec(EMBEDDING_SIZE)

# ╔═╡ 985a5fe6-db00-470c-b93d-e1eeae54d206
params(MFMLPRec)

# ╔═╡ 5200ae0d-54b5-4b31-b8c8-5e72bfc06469
md"""
##### 3.3 Matrix Factorization Concatenation Multi-Layer Perceptron Model

Testing this out
"""

# ╔═╡ 135fe920-005b-42d9-85de-9a52afba600c
function _create_MFConcatMLP(embedding_size)
    student_embedding, learning_object_embedding = _create_embeddings(embedding_size)
    concatenation(S, T) =
        vcat(student_embedding(S), learning_object_embedding(T)) |> gpu_or_cpu
end

# ╔═╡ 0b82849e-db1e-437b-afe0-c1f4128670bd
MFConcatMLP = _create_MFConcatMLP(EMBEDDING_SIZE)

# ╔═╡ 7aa90fa0-3001-4363-a018-47f561c5c85a
function _create_MFConcatMLPRec(embedding_size)
    function print_data(x)
        println(
            "size = $(size(x)), size1 = $(size(x, 1)), size1 = $(size(x, 2)), length = $(length(x)), ndims = $(ndims(x))",
        )
        x
    end
    Chain(
        x -> MFConcatMLP(x[1], x[2]),
        flatten,
        x -> Dense(size(x, 1), 1)(x),
        x -> Dense(size(x', 1), 1)(x'),
        print_data,
        _create_MFMLP(embedding_size),
    ) |> gpu_or_cpu
end

# ╔═╡ b496adc7-e74d-43a5-b732-327654cbda29
MFConcatMLPRec = _create_MFConcatMLPRec(EMBEDDING_SIZE)

# ╔═╡ c5b5f83d-1793-4fa0-acfb-e74ee1f194a2
m = MFConcatMLPRec([[1], [1]])

# ╔═╡ b4a40575-7e3d-4ff4-8974-ca8b730788cc
md"""
##### 3.4 Pure Multi-Layer Perceptron Model
"""

# ╔═╡ e7f9ff72-fdaa-423b-bfbd-4b8cdfb3ae79
function _create_MLP(embedding_size, use_batchnorm = true)
    if use_batchnorm
        Chain(
            Embedding(1, embedding_size, identity),
            BatchNorm(embedding_size),
            Dropout(0.2),
            Dense(embedding_size, 256, relu),
            BatchNorm(256),
            Dropout(0.2),
            Dense(256, 64, relu),
            BatchNorm(64),
            Dropout(0.2),
            Dense(64, 1, sigmoid),
        ) |> gpu_or_cpu
    else
        Chain(
            Embedding(1, embedding_size, identity),
            Dropout(0.2),
            Dense(embedding_size, 256, relu),
            Dropout(0.2),
            Dense(256, 64, relu),
            Dropout(0.2),
            Dense(64, 1, sigmoid),
        ) |> gpu_or_cpu
    end
end

# ╔═╡ f394671d-6e68-478e-9459-63443855ad58
function _create_MLPRec(embedding_size)
    Chain(x -> [MFRec(x[1], x[2])], flatten, _create_MFMLP(EMBEDDING_SIZE)) |> gpu_or_cpu
end

# ╔═╡ e18e25b5-70d3-46a5-9b89-47892ebae80b
MLPRec = _create_MLPRec(EMBEDDING_SIZE)

# ╔═╡ e70e23fe-74e7-4a4b-88de-e567d105a995
md"""
##### 3.5 Simplified Pure Multi-Layer Perceptron Model
"""

# ╔═╡ 134872ff-1d54-40f8-9d04-07b61a45760d
function _create_SimplifiedMLPRec(embedding_size, use_batchnorm = true)
    if use_batchnorm
        Chain(
            Embedding(2, embedding_size, identity),
            BatchNorm(embedding_size),
            Dropout(0.2),
            Dense(embedding_size, 256, relu),
            BatchNorm(256),
            Dropout(0.2),
            Dense(256, 64, relu),
            BatchNorm(64),
            Dropout(0.2),
            Dense(64, 1, sigmoid),
        ) |> gpu_or_cpu
    else
        Chain(
            Embedding(2, embedding_size, identity),
            Dropout(0.2),
            Dense(embedding_size, 256, relu),
            Dropout(0.2),
            Dense(256, 64, relu),
            Dropout(0.2),
            Dense(64, 1, sigmoid),
        ) |> gpu_or_cpu
    end
end

# ╔═╡ 773a2047-1b8c-42f3-85e5-3cf20c88a5f3
md"""
##### 3.7 Create Auto Encoder Model
"""

# ╔═╡ 5dd65acb-0272-437b-ad74-a304e986f418
function _create_AERec(input_size = 3)
    # Latent dimensionality, # hidden units.
    a, b, c, d, e, f = input_size, 1024, 512, 256, 64, 32
    encoder_layer_1 = Dense(a, b, relu)
    encoder_layer_2 = Dense(b, c, relu)
    encoder_layer_3 = Dense(c, d, relu)
    encoder_layer_4 = Dense(d, e, relu)
    encoder_layer_5 = Dense(e, f, relu)

    decoder_layer_1 = Dense(f, e, relu)
    decoder_layer_2 = Dense(e, d, relu)
    decoder_layer_3 = Dense(d, c, relu)
    decoder_layer_4 = Dense(c, b, relu)
    decoder_layer_5 = Dense(b, a)

    # encoder
    encoder = Chain(
        encoder_layer_1,
        encoder_layer_2,
        encoder_layer_3,
        encoder_layer_4,
        encoder_layer_5,
    )

    # decoder
    decoder = Chain(
        decoder_layer_1,
        decoder_layer_2,
        decoder_layer_3,
        decoder_layer_4,
        decoder_layer_5,
    )

    Chain(encoder, decoder) |> gpu_or_cpu
end

# ╔═╡ 7d6c2893-0022-4522-a21c-48a530b25a99
AERec = _create_AERec(AUTOENCODER_INPUT_SIZE)

# ╔═╡ 7884b989-6ea7-4b2b-aa01-eaa0a6256351
flatten(AERec([123 112 154; 123 123 145; 125 215 345; 125 124 235]'))

# ╔═╡ 08282003-d3dd-4e62-92fc-ed58067c50d7
A = [123 112 154; 123 123 145; 125 215 345; 125 124 235]'

# ╔═╡ 65e2ab92-0c3b-43f8-a209-d82c9a4e0afd
md"""
### Preparing Data For `Flux.jl` Ingestion

```julia
A = [123 112 154; 123 123 145; 125 215 345; 125 124 235]
println(A)
```

Executing the code above produces the following output

```julia
A =  4×3 Array{Int64,2}:
      123  112  154
      123  123  145
      125  215  345
      125  124  235
```

Use `A'` or `transpose(A)` to ingest the vectors into a Flux as a 3xn vectors


```julia
A' = 3×4 LinearAlgebra.Adjoint{Int64,Array{Int64,2}}:
     123  123  125  125
     112  123  215  124
     154  145  345  235
```

"""


# ╔═╡ 8eaf39c2-087c-4e82-8878-0a586f263d61
md"""
### References
1. [Deep Learning With Keras: Recommender Systems](https://www.johnwittenauer.net/deep-learning-with-keras-recommender-systems/)
2. [Movie Recommendation system(For Deployment)](https://www.kaggle.com/terminate9298/movie-recommendation-system-for-deployment#Collaborative-Filtering--Based-Recommendation-System)
3. [Neural Collaborative Filtering](https://calvinfeng.gitbook.io/machine-learning-notebook/supervised-learning/recommender/neural_collaborative_filtering)
"""

# ╔═╡ Cell order:
# ╠═0e56c163-c59f-45c5-9b2e-5d0faa140b18
# ╠═aa91e604-e08e-41f1-9be7-2d2ebf7d7f17
# ╠═5480672b-95bc-430c-a58d-ec51454d1c2c
# ╟─83980722-1cf8-4a08-bc63-f9f161ce6848
# ╠═80e70c08-78cc-446e-b6bc-788c84149405
# ╟─427ef58e-b6d7-4208-a503-4114cb6d173f
# ╟─18165846-7922-4f71-9456-113882670229
# ╟─c9937231-4aa9-401d-bacd-826edce74cae
# ╟─0671a158-1b71-4def-bee7-b10247255de8
# ╟─203f5f37-b5c9-4c28-9a17-11e9e1247db0
# ╟─8a630bab-32d3-4f6b-af67-1b8221b117df
# ╟─61a88885-587b-4443-9ba3-367a9fd995e5
# ╠═6149bcff-f158-4a55-a832-b3b57c45ccd8
# ╟─eca69416-c4f2-4355-8d54-a8b28d2c5e4d
# ╟─c551cfe1-d545-447a-8b03-8d64bd394d79
# ╟─03ccfe70-3914-4d2e-81f2-9bbaf6d5c0bd
# ╠═2e6a74db-caf2-45ec-8a25-dbf6cc7c6302
# ╠═fa0eac79-1720-49b9-82ca-f6f2ffc06a10
# ╠═17efd88b-ff51-4674-914e-bf1dc5d14bb0
# ╠═6df1d68c-039c-4d98-84c0-4124234e29c7
# ╠═0d2ada1f-d8fb-40eb-8b21-48083ff63901
# ╠═ec12425f-d267-4886-82e7-f193af0c8adb
# ╠═211a2da1-cb99-425a-97d6-a2abe1a382ba
# ╠═4448c7bc-11c2-4ff5-a9ab-511f658b85ff
# ╟─a9a4eccd-3d8a-4a9b-a1eb-9cb8721db7c4
# ╠═5dc59ffb-68fd-4b71-b8fc-2bde561ff90e
# ╠═d9a686dc-4acf-44e4-9186-35febf9b5246
# ╠═f05a2888-1a08-484d-9473-60dcca438236
# ╠═985a5fe6-db00-470c-b93d-e1eeae54d206
# ╠═5200ae0d-54b5-4b31-b8c8-5e72bfc06469
# ╠═135fe920-005b-42d9-85de-9a52afba600c
# ╠═0b82849e-db1e-437b-afe0-c1f4128670bd
# ╠═7aa90fa0-3001-4363-a018-47f561c5c85a
# ╠═b496adc7-e74d-43a5-b732-327654cbda29
# ╠═c5b5f83d-1793-4fa0-acfb-e74ee1f194a2
# ╠═b4a40575-7e3d-4ff4-8974-ca8b730788cc
# ╠═e7f9ff72-fdaa-423b-bfbd-4b8cdfb3ae79
# ╠═f394671d-6e68-478e-9459-63443855ad58
# ╠═e18e25b5-70d3-46a5-9b89-47892ebae80b
# ╠═e70e23fe-74e7-4a4b-88de-e567d105a995
# ╠═134872ff-1d54-40f8-9d04-07b61a45760d
# ╠═773a2047-1b8c-42f3-85e5-3cf20c88a5f3
# ╠═5dd65acb-0272-437b-ad74-a304e986f418
# ╠═7d6c2893-0022-4522-a21c-48a530b25a99
# ╠═7884b989-6ea7-4b2b-aa01-eaa0a6256351
# ╠═08282003-d3dd-4e62-92fc-ed58067c50d7
# ╠═65e2ab92-0c3b-43f8-a209-d82c9a4e0afd
# ╟─8eaf39c2-087c-4e82-8878-0a586f263d61
