using Documenter, APLORS

makedocs(;
    modules = [APLORS],
    format = Documenter.HTML(),
    pages = ["Home" => "index.md"],
    repo = "https://gitlab.com/stalinkay/APLORS.jl/blob/{commit}{path}#L{line}",
    sitename = "APLORS.jl",
    authors = "stalinkay",
    assets = String[],
)
