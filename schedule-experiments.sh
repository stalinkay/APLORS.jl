#!/usr/bin/env bash

cd experiment
# qsub experiment-1-100k-ncf-2-nodes-1-cpus.pbs
# qsub experiment-1-100k-ncf-2-nodes-2-cpus.pbs
# qsub experiment-2-100k-all-models.pbs
# qsub experiment-3-100k-ncf-embedding.pbs
# qsub experiment-4-100k-ncf-batch-size.pbs
# qsub experiment-5-100k-ncf-learning-rate.pbs
# qsub experiment-6-100k-ncf-classification.pbs
# qsub experiment-7-100k-ncf-batch-norm.pbs
# qsub experiment-9-100k-ncf-l2.pbs
# qsub experiment-10-100k-ncf-optimizer.pbs
# qsub experiment-11-100k-ncf-use-sigmoid.pbs

# qsub experiment-12-1m-ncf-sigmoid-0-embedding-8-learning-rate-0.001-batch-size-16.pbs
# qsub experiment-13-9m-ncf-sigmoid-0-embedding-8-learning-rate-0.001-batch-size-16.pbs

qsub experiment-12-1m-ncf-sigmoid-0-embedding-8-learning-rate-0.001-batch-size-16-test-size-0.3.pbs
qsub experiment-13-9m-ncf-sigmoid-0-embedding-8-learning-rate-0.001-batch-size-16-test-size-0.3.pbs

qsub experiment-12-1m-ncf-sigmoid-0-learning-rate-0.001-batch-size-16.pbs
qsub experiment-13-9m-ncf-sigmoid-0-learning-rate-0.001-batch-size-16.pbs

qsub experiment-12-1m-ncf.pbs
qsub experiment-13-9m-ncf.pbs

qsub experiment-13-9m-ncf-sigmoid-0-embedding-128-learning-rate-0.001-batch-size-16.pbs