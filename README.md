# APLORS

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://stalinkay.gitlab.io/APLORS.jl/dev)
[![Build Status](https://gitlab.com/stalinkay/APLORS.jl/badges/master/build.svg)](https://gitlab.com/stalinkay/APLORS.jl/pipelines)
[![Coverage](https://gitlab.com/stalinkay/APLORS.jl/badges/master/coverage.svg)](https://gitlab.com/stalinkay/APLORS.jl/commits/master)


## Activate Package and `Revise.jl`

```julia
julia
using Pkg
Pkg.activate(".")
using Revise

# Format code
using JuliaFormatter
format(".")

# Test Package
Pkg.test("APLORS")
```

## Running Tests

`julia -e "test/runtests"`