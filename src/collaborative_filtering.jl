module CF

export recommend

import BSON: @load, @save
import DataFrames: DataFrame, nrow, sort

function recommend(X::AbstractArray, model_path::AbstractString, threshold::Float64 = 0.0, k::Integer = 10, model::Union{Nothing, Function} = nothing)
    n_obs = size(X, 2)
    @assert !isempty(X) || n_obs > 0 "No data was provided to make recommendations"

    model = isnothing(model) ? load_model(model_path) : model
    @assert !isnothing(model) "No model was provided"

    predictions = model(X)
    rating_df = DataFrame(student = predictions[1, :], learning_object = predictions[2, :], rating_predicted = predictions[3, :])
    rating_df[rating_df.rating_predicted .>= threshold]
    rating_df = sort(rating_df, [order(:rating_predicted, rev = false)])

    rating_df[1:k, :] |> DataFrame
end

function load_model(model_path::AbstractString)
    @assert !isempty(model_path) "The provided model path is empty"
    model::Union{Nothing, Function} = nothing
    if model_path
        @load "$(model_path)" model
    end
    model
end

function save_model(model::Function, model_path::AbstractString)
    @assert !isempty(model_path) "The provided model path is empty"
    @save "$(model_path)" model
end

end