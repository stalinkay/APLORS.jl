module Layers
export Embedding

import Flux: @functor, create_bias, Dense, kaiming_normal, onehotbatch
import Random: AbstractRNG, GLOBAL_RNG, seed!

AbstractRNGOrNothing = Union{AbstractRNG, Nothing}

"""
A struct that holds embeddings for representing a specific data point

* params: `weight`, `bias`
"""
struct Embedding{M<:AbstractMatrix, B}
    weight::M
    bias::B
    function Embedding(W::M, bias = true) where {M<:AbstractMatrix}
        b = create_bias(W, bias, size(W,1)) # Copied
        new{M,typeof(b)}(W, b)
    end
end

create_embedding(embedding_initializer::Function, vocab_size::Integer, max_features::Integer, rng::AbstractRNGOrNothing = nothing) = isnothing(rng) ? embedding_initializer(seed!(7), max_features, vocab_size) : embedding_initializer(rng, max_features, vocab_size)

Embedding(vocab_size::Integer, max_features::Integer, nonnegative::Bool = false, bias::Bool = true, embedding_init::Function = kaiming_normal, rng::AbstractRNGOrNothing = nothing) = nonnegative ? Embedding(abs.(create_embedding(embedding_init, vocab_size, max_features, rng)), bias) : Embedding(create_embedding(embedding_init, vocab_size, max_features, rng), bias) # convenience method
(m::Embedding)(x::AbstractVecOrMat) = m.weight * onehotbatch(x, 1:size(m.weight, 2)) .+ m.bias # Use Embedding as a function by overloading
(m::Embedding)(x::AbstractArray) = reshape(m(reshape(x, size(x,1), :)), :, size(x)[2:end]...) # Use Embedding as a function by overloading
@functor Embedding # make Embedding trainable

end