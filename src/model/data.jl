module DataPattern

export train_validation_split

export DataEncoding, DistanceMetric, NLPAlgorithm, add_bias, create_ae_data, create_mf_data, load_dataset, ae_data, mf_data, read_data_frame, find_candidate_learning_objects_by_query, prepare_learning_objects, prepare_students, get_student_course_enrollments, prepare_student_learning_objects, get_student_rating_for_learning_object, prepare_learning_objects_for_student, prepare_content_data

import CSV: File
import DataFrames: AbstractDataFrame, ByRow, DataFrame, combine, groupby, innerjoin, nrow, Not, order, reduce, select, select!, sort, transform!, vcat, view
import Distances: CorrDist, CosineDist, Euclidean, evaluate

# import LearnBase: ObsDim
import LinearAlgebra: Diagonal

import MLDataUtils: splitobs, shuffleobs
import MLLabelUtils: LabelEnc, convertlabel, ind2label
import NearestNeighbors: BruteTree, knn

import Random: AbstractRNG, GLOBAL_RNG, shuffle

# import ScikitLearn: @sk_import
# @sk_import preprocessing:LabelEncoder

# import ScikitLearn.CrossValidation: train_test_split
import Statistics: cor, mean

import TextAnalysis: Corpus, DocumentTermMatrix, StringDocument, dtm, lda, lsa, prepare!, remove_case!, standardize!, stem!, strip_punctuation, strip_articles, update_inverse_index!, update_lexicon!, tf, tf_idf

@enum DistanceMetric correlation cosine euclidean pearson
@enum NLPAlgorithm tf_algo lsa_uv_algo lsa_usv_algo lda_algo

AbstractRNGOrNothing = Union{AbstractRNG, Nothing}
IntegerOrNothing = Union{Integer, Nothing}
AbstractDataFrameOrAbstractArray = Union{AbstractDataFrame, AbstractArray}
RANDOM_STATE_DEFAULT = 7

"""
A struct that encapsulates data label encodings, number of students and number of learning objects

* params: `student_encoder`, `learning_object_encoder`, `n_students` and `n_learning_objects`
"""
struct DataEncoding{T}
    student_encoder::T
    learning_object_encoder::T
    n_students::Integer
    n_learning_objects::Integer
end

"""
A struct that includes all training, test and validation data

* params: `X_train`, `X_test`, `y_train`, `y_test`, `X_validation`, `y_validation`, `training`, `validation`, `data_encoding`
"""
struct Data
    X_train::AbstractVecOrMat
    X_test::AbstractVecOrMat
    y_train::AbstractVecOrMat
    y_test::AbstractVecOrMat
    X_validation::AbstractVecOrMat
    y_validation::AbstractVecOrMat
    training::AbstractDataFrame
    validation::AbstractDataFrame
    n_students::Integer
    n_learning_objects::Integer
end

"""
Split [`DataFrame`](https://dataframes.juliadata.org/stable/) or [`AbstractArray`](https://docs.julialang.org/en/v1/base/arrays/). By default, the resulting training and validation [`DataFrames`](https://dataframes.juliadata.org/stable/) are 90% of the original [`DataFrame`](https://dataframes.juliadata.org/stable/).

* Shuffles the provided [`DataFrame`](https://dataframes.juliadata.org/stable/) if an `rng` is provided the original `df` is shuffled using the rng for reproduciblity
* Splits [`DataFrame`](https://dataframes.juliadata.org/stable/) or [`AbstractArray`](https://docs.julialang.org/en/v1/base/arrays/) using [`MLDataPattern`](https://mldatautilsjl.readthedocs.io/en/latest/data/pattern.html#splitting-into-train-test)
* Returns the training and validation [`DataFrames`](https://dataframes.juliadata.org/stable/)

* params: `df`, `at`, `rng`
"""
function train_validation_split(df::Union{AbstractDataFrame, AbstractArray}, at::Float64 = 0.9, rng::AbstractRNGOrNothing = nothing)
    @assert 0 <= at <= 1 "At should be a value between 0 and 1"
    ids = collect(axes(df, 1))
    ids = isnothing(rng) ? ids : shuffle(rng, ids)
    sel = ids .<= nrow(df) .* at
    view(df, sel, :) |> DataFrame, view(df, .!sel, :) |> DataFrame
    # train, validation = isnothing(rng) ? splitobs(df, at = at, obsdim =  :first) : splitobs(shuffleobs(df, obsdim =  :first, rng = rng); at = at)
    # train, validation
end

"""
Read a [`CSV`](https://csv.juliadata.org/stable/) dataset at a given `path`

* Returns the resulting [`DataFrame`](https://dataframes.juliadata.org/stable/) and [`DataEncoding`](@ref)

* params: `path`
"""
read_data_frame(path::AbstractString) = File(path) |> DataFrame

"""
Load a [`CSV`](https://csv.juliadata.org/stable/) dataset at a given `path`

* Loads dataset at a given `path`
* Shuffles dataset using `RNG` if provided
* Extracts all or only the first n number of observations/rows if `nobs` is provided
* Adds student and learning object encodings to the returned [`DataFrame`](https://dataframes.juliadata.org/stable/)
* Returns the resulting [`DataFrame`](https://dataframes.juliadata.org/stable/) and [`DataEncoding`](@ref)

* params: `path`, `rng`, `nobs`
"""
function load_dataset(path::AbstractString, rng::AbstractRNGOrNothing = nothing, nobs::IntegerOrNothing = nothing)
    df = read_data_frame(path)
    df = isnothing(rng) ? df : df[shuffle(rng, 1:nrow(df)), :]
    nobs = isnothing(nobs) ? nrow(df) : nobs
    df = first(df, nobs)

    students = sort(unique(df[!, :student_id]))
	student_enc = LabelEnc.NativeLabels(students)
	df[!, :student] .= convertlabel(LabelEnc.Indices, df[!, :student_id], student_enc)
    n_students = length(students)

	learning_objects = sort(unique(df[!, :learning_object_id]))
	learning_object_enc = LabelEnc.NativeLabels(learning_objects)
	df[!, :learning_object] .= convertlabel(LabelEnc.Indices, df[!, :learning_object_id], learning_object_enc)
    n_learning_objects = length(learning_objects)

    df, n_students, n_learning_objects

    # student_enc = LabelEncoder()
    # df[!, :student] = fit_transform!(student_enc, df[!, :student_id])
    # n_students = length(unique(df[!, :student]))

    # learning_object_enc = LabelEncoder()
    # df[!, :learning_object] = fit_transform!(learning_object_enc, df[!, :learning_object_id])
    # n_learning_objects = length(unique(ratings[!, :learning_object]))

    # data_encoding = DataEncoding(student_enc, learning_object_enc, n_students, n_learning_objects)
    # df, data_encoding
end

"""
Compute and add biases to a given [`DataFrame`](https://dataframes.juliadata.org/stable/)

* Add rating mean, `:μ`
* Add student bias, `:student_rating_bias`
* Add learning object bias, `:learning_object_rating_bias`
* Returns the original [`DataFrame`](https://dataframes.juliadata.org/stable/) with computed mean and biases added

* params: `df`
"""
function add_bias(df::DataFrame)
    df[!, :μ] .= mean(df[!, :rating])

    # change ratings to 1 to 6
    ratings = sort(unique(df[!, :rating]))
	rating_enc = LabelEnc.NativeLabels(ratings)
	df[!, :rating] .= convertlabel(LabelEnc.Indices, df[!, :rating], rating_enc)

    # add student bias
    gdf = groupby(df, :student_id)
    means = combine(gdf, :rating => mean => :student_rating_bias)
    df = innerjoin(df, means, on = :student_id)

    # add learning object bias
    gdf = groupby(df, :learning_object_id)
    means = combine(gdf, :rating => mean => :learning_object_rating_bias)
    df = innerjoin(df, means, on = :learning_object_id)

    # add student learning object bias
    gdf = groupby(df, [:student_id, :learning_object_id])
    means = combine(gdf, :rating => mean => :student_learning_object_rating_bias)
    df = innerjoin(df, means, on = [:student_id, :learning_object_id])

    df[!, :rating_normalized] .= df[!, :μ] - df[!, :student_rating_bias] - df[!, :learning_object_rating_bias]

    df
end

# """
# Create Matrix Factorization data given a [`DataFrame`](https://dataframes.juliadata.org/stable/) with `:student`, `:learning_object` and `:rating_normalized`

# * Selects `:student` and `:learning_object` as X, input
# * Selects `:rating_normalized` as y, desired output for supervised learning
# * Returns a `Tuple` of `X_train`, `X_test`, `y_train`, `y_test`

# * params: `X_train`, `X_test`, `y_train`, `y_test`
# """
# function create_mf_data(df::AbstractDataFrame, test_size::Float64 = 0.1, random_state::Integer = RANDOM_STATE_DEFAULT, shuffle::Bool = false)
#     X = Matrix(select(df, [:student, :learning_object]))
#     y = Matrix(select(df, [:rating_normalized]))
#     # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = test_size, random_state = random_state, shuffle = shuffle)
#     # X_train, X_test, y_train, y_test
# end

# """
# Create Matrix Factorization data for AutoEncoder training given a [`DataFrame`](https://dataframes.juliadata.org/stable/) with `:student`, `:learning_object` and `:rating_normalized`

# * Selects `:student`, `:learning_object`, `:rating_normalized`and  as X: input
# * Selects X, desired output for supervised learning
# * Returns a `Tuple` of `X_train`, `X_test`, `y_train`, `y_test`

# * params: `X_train`, `X_test`, `y_train`, `y_test`
# """
# function create_ae_data(df::AbstractDataFrame, test_size::Float64 = 0.1, random_state::Integer = RANDOM_STATE_DEFAULT, shuffle::Bool = false)
#     X = Matrix(select(df, [:student, :learning_object, :rating_normalized]))
#     # X_train, X_test, y_train, y_test = train_test_split(X, X, test_size = test_size, random_state = random_state, shuffle = shuffle)
#     # X_train, X_test, y_train, y_test
# end

"""
Create Matrix Factorization data for training given a `path` using [`create_mf_data`](@ref)

* Loads dataset at `path` using [`load_dataset`](@ref)
* Adds biases using [`mf_data`](@ref) 
* Splits [`DataFrame`](https://dataframes.juliadata.org/stable/) into a `training` and `validation` using [`train_validation_split`](@ref)
* Splits `training` data into a `X_train`, `X_test`, `y_train`, `y_test` using [`create_mf_data`](@ref)
* Splits `validation` data into a `X_validation`, `y_validation` using [`create_mf_data`](@ref)
* Returns [`Data`](@ref)

* params: `path`, `rng`, `nobs`, `test_size`, `random_state`, `shuffle`
"""
function mf_data(path::AbstractString, rng::AbstractRNGOrNothing = nothing, nobs::IntegerOrNothing = nothing, test_size::Float64 = 0.1, random_state::Integer = RANDOM_STATE_DEFAULT, shuffle::Bool = false)
    df, n_students, n_learning_objects = load_dataset(path, rng, nobs)
    df = add_bias(df)
    training_df, validation_df = train_validation_split(df, 1.0 - test_size, rng)
    # X_train, X_test, y_train, y_test = create_mf_data(training_df, test_size, random_state, shuffle)
    # X_validation, y_validation, _, _ = create_mf_data(validation_df, 0.0, random_state, shuffle) # no test_size
    # Data(X_train, X_test, y_train, y_test, X_validation, y_validation, training_df, validation_df, n_students, n_learning_objects)

    training_df, validation_df, n_students, n_learning_objects
end

"""
Create Matrix Factorization data for AutoEncoder training given a `path` using [`create_mf_data`](@ref)

* Loads dataset at `path` using [`load_dataset`](@ref)
* Adds biases using [`mf_data`](@ref)
* Splits [`DataFrame`](https://dataframes.juliadata.org/stable/) into a `training` and `validation` using [`train_validation_split`](@ref)
* Splits `training` data into a `X_train`, `X_test`, `y_train`, `y_test` using [`create_mf_data`](@ref)
* Splits `validation` data into a `X_validation`, `y_validation` using [`create_mf_data`](@ref)
* Returns [`Data`](@ref)

* params: `path`, `rng`, `nobs`, `test_size`, `random_state`, `shuffle`
"""
function ae_data(path::AbstractString, rng::AbstractRNGOrNothing = nothing, nobs::IntegerOrNothing = nothing, test_size::Float64 = 0.1, random_state::Integer = RANDOM_STATE_DEFAULT, shuffle::Bool = false)
    df, n_students, n_learning_objects = load_dataset(path, rng, nobs)
    df = add_bias(df)
    training_df, validation_df = train_validation_split(df, 1 - test_size, rng)
    # X_train, X_test, y_train, y_test = create_ae_data(training_df, test_size, random_state, shuffle)
    # X_validation, y_validation, _, _ = create_ae_data(validation_df, 0, random_state, shuffle) # no test_size
    # Data(X_train, X_test, y_train, y_test, X_validation, y_validation, training_df, validation_df, n_students, n_learning_objects)
end

function create_new_symbol(symbol::Symbol, suffix::AbstractString)
    Symbol(symbol, suffix)
end

function get_course_prequisites(course_prerequisite_df, course::AbstractString, course_id_column::Symbol)
    unique(course_prerequisite_df[course_prerequisite_df[!, course_id_column] .== course,  :prerequisite])
end

function prepare_courses(course_df, course_prerequisite_df, course_id_column::Symbol = :course_id, rng::AbstractRNGOrNothing = nothing)
    # course_gdf = groupby(course_df, course_id_column)
    # prerequisite_df = combine(course_gdf, :course_id .=> (courses -> [get_course_prequisites(course_prerequisite_df, course, course_id_column) for course in courses]) .=> :prerequisite)
    course_df[!, :course_prerequisite_id] .= [get_course_prequisites(course_prerequisite_df, course, course_id_column) for course in course_df[!, course_id_column]]
    course_df = isnothing(rng) ? course_df : course_df[shuffle(rng, 1:nrow(course_df)), :]
end

function get_topic_prequisites(topic_prerequisite_df, topic::Int64, topic_id_column::Symbol)
    unique(topic_prerequisite_df[topic_prerequisite_df[!, topic_id_column] .== topic,  :prerequisite])
end

function prepare_topics(topic_df, topic_prerequisite_df, topic_id_column::Symbol = :topic_id, rng::AbstractRNGOrNothing = nothing)
    # topic_gdf = groupby(topic_df, topic_id_column)
    # prerequisite_df = combine(topic_gdf, :topic_id .=> (topics -> [get_topic_prequisites(topic_prerequisite_df, topic, topic_id_column) for topic in topics]) .=> :prerequisite)
    topic_df[!, :topic_prerequisite_id] .= [get_topic_prequisites(topic_prerequisite_df, topic, topic_id_column) for topic in topic_df[!, topic_id_column]]
    topic_df = isnothing(rng) ? topic_df : topic_df[shuffle(rng, 1:nrow(topic_df)), :]
end

function get_student_mean_rating_for_learning_objects(student::Integer, student_learning_object_rating_df::DataFrame)
	df = student_learning_object_rating_df[student_learning_object_rating_df.student_id .== student, [:student_id, :learning_object_id, :rating, :student_rating_bias]] |> DataFrame
	nrow(df) > 0 ? mean(df.rating) : 0
end

function get_student_rating_for_learning_object(student::Integer, learning_object::Integer, student_learning_object_rating_df::DataFrame)
	df = student_learning_object_rating_df[(student_learning_object_rating_df.student_id .== student) .& (student_learning_object_rating_df.learning_object_id .== learning_object), [:student_id, :learning_object_id, :rating, :student_rating_bias]] |> DataFrame
	nrow(df) > 0 ? mean(df.rating) : get_student_mean_rating_for_learning_objects(student, student_learning_object_rating_df)
end

function create_learning_objects(learning_object_df::DataFrame, course_df::DataFrame, topic_df::DataFrame, rng::AbstractRNGOrNothing = nothing)
    learning_object_df = innerjoin(learning_object_df, course_df, on = :course_id) |> DataFrame
    learning_object_df = innerjoin(learning_object_df, topic_df, on = [:topic_id, :course_id]) |> DataFrame
    learning_object_df = isnothing(rng) ? learning_object_df : learning_object_df[shuffle(rng, 1:nrow(learning_object_df)), :]
end

function create_learning_object_descriptions(learning_object_df::DataFrame, rng::AbstractRNGOrNothing = nothing)
    learning_object_df[!, :description] = string.(learning_object_df[!, :title], " ", learning_object_df[!, :objectives], " ", learning_object_df[!, :learning_object_type])
    learning_object_df = isnothing(rng) ? learning_object_df : learning_object_df[shuffle(rng, 1:nrow(learning_object_df)), :]
end

function prepare_learning_objects(learning_object_df::DataFrame, course_df::DataFrame, course_prerequisite_df::DataFrame, topic_df::DataFrame, topic_prerequisite_df::DataFrame, nobs::IntegerOrNothing = nothing, rng::AbstractRNGOrNothing = nothing)
    course_df = prepare_courses(course_df, course_prerequisite_df, :course_id, rng)
    topic_df = prepare_topics(topic_df, topic_prerequisite_df, :topic_id, rng)
    learning_object_df = create_learning_objects(learning_object_df, course_df, topic_df, rng)
    learning_object_df = create_learning_object_descriptions(learning_object_df, rng)
    select!(learning_object_df, Not(:title))
    select!(learning_object_df, Not(:objectives))
    
    nobs = isnothing(nobs) ? nrow(learning_object_df) : nobs
    learning_object_df = first(learning_object_df, nobs)
    learning_object_df = isnothing(rng) ? learning_object_df : learning_object_df[shuffle(rng, 1:nrow(learning_object_df)), :]

    learning_object_df
end

function get_student_course_enrollments(student_course_enrollment_df::DataFrame, student::Int64, student_id_column::Symbol)
    unique(student_course_enrollment_df[student_course_enrollment_df[!, student_id_column] .== student, :course_id])
end

function get_student_preferred_topics(student_preferred_topic_df::DataFrame, student::Int64, student_id_column::Symbol)
    unique(student_preferred_topic_df[student_preferred_topic_df[!, student_id_column] .== student, :topic_id])
end

function create_students(student_df::DataFrame, course_df::DataFrame, student_preferred_topic_df::DataFrame, student_course_enrollment_df::DataFrame, student_id_column::Symbol = :student_id, rng::AbstractRNGOrNothing = nothing)
    student_df = innerjoin(student_df, course_df, on = :course_id) |> DataFrame
    # student_gdf = groupby(student_df, student_id_column)

    # preferred_topic_df = combine(student_gdf, :student_id .=> (students -> [get_student_preferred_topics(student_preferred_topic_df, student, student_id_column) for student in students]) .=> :topic_id)
    student_df[!, :topic_id] .= [get_student_preferred_topics(student_preferred_topic_df, student, student_id_column) for student in student_df[!, :student_id]]

    # student_preferred_topic_gdf = groupby(student_preferred_topic_df, student_id_column)
    # course_enrollment_df = combine(student_gdf, :student_id .=> (students -> [get_student_course_enrollments(student_course_enrollment_df, student, student_id_column) for student in students]) .=> :course_id)
    # println("student_df = $(nrow(student_df)), course_enrollment_df = $(nrow(course_enrollment_df))")

    student_df[!, :course_id] .= [get_student_course_enrollments(student_course_enrollment_df, student, student_id_column) for student in student_df[!, :student_id]]
    student_df = isnothing(rng) ? student_df : student_df[shuffle(rng, 1:nrow(student_df)), :]
end

function prepare_students(student_df::DataFrame, student_preferred_topic_df::DataFrame, student_course_enrollment_df::DataFrame, course_df::DataFrame, course_prerequisite_df::DataFrame, nobs::IntegerOrNothing = nothing, rng::AbstractRNGOrNothing = nothing)
    course_df = prepare_courses(course_df, course_prerequisite_df, :course_id, rng) |> DataFrame
    student_df = create_students(student_df, course_df, student_preferred_topic_df, student_course_enrollment_df, :student_id, rng) |> DataFrame

    nobs = isnothing(nobs) ? nrow(student_df) : nobs
    student_df = first(student_df, nobs)
    student_df = isnothing(rng) ? student_df : student_df[shuffle(rng, 1:nrow(student_df)), :]
end

function add_missing_columns(df1::DataFrame, df2::DataFrame)
    # Add missing columns before concatenating dataframes
    for n in unique([propertynames(df1); propertynames(df2)]), df in [df1, df2]
        n in propertynames(df) || (df[!, n] .= missing)
    end
    df1, df2
end

function prepare_student_learning_objects(student_df::DataFrame, learning_object_df::DataFrame)
    # learning_object_df, student_df = add_missing_columns(learning_object_df, student_df)
    df = reduce(vcat, [student_df, learning_object_df], cols = :union, source = nothing)

    # learning_object_types = unique(df[!, :learning_object_type])
    # enc = LabelEnc.NativeLabels(learning_object_types)
    # df[!, :learning_object_type_enc] = convertlabel(LabelEnc.Indices, df[!, :learning_object_type], enc)
    df
end

function prepare_learning_objects_for_student(student::Integer, student_df::DataFrame, learning_object_df::DataFrame, student_learning_object_df::DataFrame)
	learning_object_df[!, :rating] .= [get_student_rating_for_learning_object(student, lo, student_learning_object_df) for lo in learning_object_df[!, :learning_object_id]]
	student_df = student_df[student_df.student_id .== student, :]

	student_df[!, :rating] .= [get_student_mean_rating_for_learning_objects(student, student_learning_object_df) for student in student_df[!, :student_id]]
	
	learning_object_df, student_df
end

function prepare_content_data(content_df::DataFrame, student_df::DataFrame, learning_object_df::DataFrame, replace_cols::Array{Symbol})
	# learning object types
	learning_object_types = sort(unique(content_df[!, :learning_object_type]))
	learning_object_type_enc = LabelEnc.NativeLabels(learning_object_types)
	content_df[!, :learning_object_type] .= convertlabel(LabelEnc.Indices, content_df[!, :learning_object_type], learning_object_type_enc)
	content_df[!, :lo_label] .= ind2label.(content_df[!, :learning_object_type], learning_object_type_enc)
	
	# week from
	week_from = sort(unique(content_df[!, :week_from]))
	week_from_enc = LabelEnc.NativeLabels(week_from)
	content_df[!, :week_from] .= convertlabel(LabelEnc.Indices, content_df[!, :week_from], week_from_enc)
	
	# week to
	week_to = sort(unique(content_df[!, :week_to]))
	week_to_enc = LabelEnc.NativeLabels(week_to)
	content_df[!, :week_to] .= convertlabel(LabelEnc.Indices, content_df[!, :week_to], week_to_enc)
	
	# presentation duration
	presentation_duration = sort(unique(content_df[!, :presentation_duration]))
	presentation_duration_enc = LabelEnc.NativeLabels(presentation_duration)
	content_df[!, :presentation_duration] .= convertlabel(LabelEnc.Indices, content_df[!, :presentation_duration], presentation_duration_enc)
	
	# gender
	gender = sort(unique(content_df[!, :gender]))
	gender_enc = LabelEnc.NativeLabels(gender)
	content_df[!, :gender] .= convertlabel(LabelEnc.Indices, content_df[!, :gender], gender_enc)
	
	# region
	region = sort(unique(content_df[!, :region]))
	region_enc = LabelEnc.NativeLabels(region)
	content_df[!, :region] .= convertlabel(LabelEnc.Indices, content_df[!, :region], region_enc)
	
	# highest education
	highest_education = sort(unique(content_df[!, :highest_education]))
	highest_education_enc = LabelEnc.NativeLabels(highest_education)
	content_df[!, :highest_education] .= convertlabel(LabelEnc.Indices, content_df[!, :highest_education], highest_education_enc)
	
	# imd band
	imd_band = sort(unique(content_df[!, :imd_band]))
	imd_band_enc = LabelEnc.NativeLabels(imd_band)
	content_df[!, :imd_band] .= convertlabel(LabelEnc.Indices, content_df[!, :imd_band], imd_band_enc)
	
	# age band
	age_band = sort(unique(content_df[!, :age_band]))
	age_band_enc = LabelEnc.NativeLabels(age_band)
	content_df[!, :age_band] .= convertlabel(LabelEnc.Indices, content_df[!, :age_band], age_band_enc)
	
	# num of prev attempts
	num_of_prev_attempts = sort(unique(content_df[!, :num_of_prev_attempts]))
	num_of_prev_attempts_enc = LabelEnc.NativeLabels(num_of_prev_attempts)
	content_df[!, :num_of_prev_attempts] .= convertlabel(LabelEnc.Indices, content_df[!, :num_of_prev_attempts], num_of_prev_attempts_enc)
	
	# studied credits
	studied_credits = sort(unique(content_df[!, :studied_credits]))
	studied_credits_enc = LabelEnc.NativeLabels(studied_credits)
	content_df[!, :studied_credits] .= convertlabel(LabelEnc.Indices, content_df[!, :studied_credits], studied_credits_enc)
	
	# is disability
	is_disability = sort(unique(content_df[!, :is_disability]))
	is_disability_enc = LabelEnc.NativeLabels(is_disability)
	content_df[!, :is_disability] .= convertlabel(LabelEnc.Indices, content_df[!, :is_disability], is_disability_enc)
	
	# final result
	final_result = sort(unique(content_df[!, :final_result]))
	final_result_enc = LabelEnc.NativeLabels(final_result)
	content_df[!, :final_result] .= convertlabel(LabelEnc.Indices, content_df[!, :final_result], final_result_enc)
	
	# registration date
	registration_date = sort(unique(content_df[!, :registration_date]))
	registration_date_enc = LabelEnc.NativeLabels(registration_date)
	content_df[!, :registration_date] .= convertlabel(LabelEnc.Indices, content_df[!, :registration_date], registration_date_enc)
	
	# deregistration date
	deregistration_date = sort(unique(content_df[!, :deregistration_date]))
	deregistration_date_enc = LabelEnc.NativeLabels(deregistration_date)
	content_df[!, :deregistration_date] .= convertlabel(LabelEnc.Indices, content_df[!, :deregistration_date], deregistration_date_enc)
	
	# learning object
	learning_object = sort(unique(content_df[!, :learning_object_id]))
	learning_object_enc = LabelEnc.NativeLabels(learning_object)
	content_df[!, :learning_object] .= convertlabel(LabelEnc.Indices, content_df[!, :learning_object_id], learning_object_enc)
	
	# student
	student = sort(unique(content_df[!, :student_id]))
	student_enc = LabelEnc.NativeLabels(student)
	content_df[!, :student] .= convertlabel(LabelEnc.Indices, content_df[!, :student_id], student_enc)
	
	_ = nrow(student_df)
	n_learning_objects = nrow(learning_object_df)
	n = nrow(content_df)
	
	content_df[!, :topic_prerequisite_id] .= replace(content_df.topic_prerequisite_id, missing => [])
	
	
	content_df[typeof.(content_df.course_id) .== String, :course_id] .= [[course] for course in content_df[typeof.(content_df.course_id) .== String, :course_id]]
	
	content_df[typeof.(content_df.topic_id) .== Int64, :topic_id] .= [[topic] for topic in content_df[typeof.(content_df.topic_id) .== Int64, :topic_id]]
	
	
	unique_enrollments = sort(unique(reduce(vcat, content_df.course_id)))
	transform!(content_df, :course_id .=> [ByRow(v -> x in v ? 1 : 0) for x in unique_enrollments] .=> Symbol.(:course_id__, unique_enrollments))
	
	unique_course_prerequisites = sort(unique(reduce(vcat, content_df.course_prerequisite_id)))
	transform!(content_df, :course_prerequisite_id .=> [ByRow(v -> x in v ? 1 : 0) for x in unique_course_prerequisites] .=> Symbol.(:course_prerequisite_id__, unique_course_prerequisites))
	
	
	unique_topics = sort(unique(reduce(vcat, content_df.topic_id)))
	transform!(content_df, :topic_id .=> [ByRow(v -> x in v ? 1 : 0) for x in unique_topics] .=> Symbol.(:topic_id__, unique_topics))
	
	unique_topic_prerequisites = sort(unique(reduce(vcat, content_df.topic_prerequisite_id)))
	transform!(content_df, :topic_prerequisite_id .=> [ByRow(v -> x in v ? 1 : 0) for x in unique_topic_prerequisites] .=> Symbol.(:topic_prerequisite_id__, unique_topic_prerequisites))
		
	content_df = select(content_df, Not(unique([replace_cols..., :course_id, :topic_id, :course_prerequisite_id, :topic_prerequisite_id]))) |> DataFrame

	l_df = content_df[1:n_learning_objects, :] |> DataFrame
	s_df = content_df[n_learning_objects + 1:end, :] |> DataFrame
	
	l_df, s_df
end

function create_corpus(query::AbstractString, learning_object_df::DataFrame, description_column::Symbol = :description)
    documents = [StringDocument(description) for description in learning_object_df[!, description_column]]
    query_document = StringDocument(query)
    crps = Corpus([documents..., query_document])

    # prepare crps
    standardize!(crps, StringDocument)
    [prepare!(sd, strip_punctuation | strip_articles) for sd in crps]
    stem!(crps)
    remove_case!(crps)
    update_lexicon!(crps)
    update_inverse_index!(crps)

    crps
end

function create_term_frequency_matrix(D::AbstractMatrix, use_tf_idf::Bool = true)
    T = use_tf_idf ? tf_idf(D) : tf(D)
    # T'
end

function create_document_term_matrix(crps::Corpus, nlpalgorithm::NLPAlgorithm = tf_algo, topic_k::Integer = 3, iterations::Integer = 1000, α::Float64 = 1.1, β::Float64 = 1.1)
    m = DocumentTermMatrix(crps)
    if nlpalgorithm == tf_algo
        return dtm(m, :dense)
    elseif nlpalgorithm == lda_algo
        ϕ, θ  = lda(m, topic_k, iterations, α, β)
        return θ'
    elseif nlpalgorithm == lsa_uv_algo || nlpalgorithm == lsa_usv_algo
        lsa_svd = lsa(crps)
        return nlpalgorithm == lsa_uv_algo ? lsa_svd.U * lsa_svd.Vt : lsa_svd.U * Diagonal(lsa_svd.S) * lsa_svd.Vt
    else
        return m
    end
    # D = nlpalgorithm == tf ?  : ( m)
    # D
end

function find_candidate_learning_objects_by_query(query::AbstractString, learning_object_df::DataFrame, k::Integer = 10, use_tf_idf::Bool = true, metric::DistanceMetric = cosine, nlpalgorithm::NLPAlgorithm = tf_algo, topic_k::Integer = 3, iterations = 1000, α::Float64 = 1.1, β::Float64 = 1.1)
    crps = create_corpus(query, learning_object_df)
    term_matrix = create_document_term_matrix(crps, nlpalgorithm, topic_k, iterations, α, β)
    tf = nlpalgorithm == tf_algo ? create_term_frequency_matrix(term_matrix, use_tf_idf) : term_matrix
    tf = tf'

    crps_length = length(crps)
    k = k < 1 ? 1 : k
    k = k > crps_length - 1 ? crps_length - 1 : k

    points = tf[:, 1:crps_length - 1]
    query_point = tf[:, crps_length]

    if metric == correlation
        learning_object_df[!, :correlation_distance] = [evaluate(CorrDist(), points[:, idx], query_point) for idx in 1:crps_length - 1]
        learning_object_df = sort(learning_object_df, [order(:correlation_distance, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == cosine
        learning_object_df[!, :cosine_distance] = [evaluate(CosineDist(), points[:, idx], query_point) for idx in 1:crps_length - 1]
        learning_object_df = sort(learning_object_df, [order(:cosine_distance, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == euclidean
        tree1 = BruteTree(points, Euclidean())
        idxs, dists = knn(tree1, query_point, k, true)
        learning_object_df = learning_object_df[[idxs...], :]
        learning_object_df[!, :euclidean_distance] = dists
        learning_object_df = sort(learning_object_df, [order(:euclidean_distance, rev = false)])
    elseif metric == pearson
        learning_object_df[!, :pearson_correlation] = [abs(cor(points[:, idx], query_point)) for idx in 1:crps_length - 1]
        learning_object_df = sort(learning_object_df, [order(:pearson_correlation, rev = true)])
        learning_object_df = learning_object_df[1:k, :]
    end
    learning_object_df
end
end