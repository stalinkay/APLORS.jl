module Models
export Dot, GMF, GMFBase, NMF, NCF, EnsembleMF, Sum, vecdot
export gpu_or_cpu

import ..Layers: Embedding
import Flux: @functor, Dense, Dropout, Chain, cpu, kaiming_normal, relu
import LinearAlgebra: dot
import Random: AbstractRNG, GLOBAL_RNG, seed!

gpu_or_cpu = cpu

AbstractRNGOrNothing = Union{AbstractRNG, Nothing}

"""
A model that holds `student` and `learning object embeddings`. This model computes the *inner dot product* of student and learning object, `r`.

* params: `student_embeddings`, `learning_object_embeddings`
"""
struct GMF
    student_embeddings
    learning_object_embeddings
end

GMF(n_students::Integer, n_learning_objects::Integer, embedding_size::Integer, nonnegative::Bool = false, bias::Bool = true, embeddings_initializer::Any = kaiming_normal, rng::AbstractRNGOrNothing = nothing) = GMF(Embedding(n_students, embedding_size, nonnegative, bias, embeddings_initializer, rng), Embedding(n_learning_objects, embedding_size,  nonnegative, bias, embeddings_initializer, rng))
(m::GMF)(x) = dot.(m.student_embeddings(x[1, :]), m.learning_object_embeddings(x[2, :])) |> gpu_or_cpu
@functor GMF

"""
A model that holds `student` and `learning object embeddings`. This model computes the *inner dot product* of student and learning object, `r`.

* params: `student_embeddings`, `learning_object_embeddings`
"""
struct GMFBase
    student_embeddings
    learning_object_embeddings
end

(m::GMFBase)(x) = dot.(m.student_embeddings(x[1, :]), m.learning_object_embeddings(x[2, :])) |> gpu_or_cpu
@functor GMFBase

vecdot(x, y) = dot.(x, y)

"""
A model that holds `student` and `learning object embeddings`. This model computes the *inner dot product* of student and learning object, `r`.

* params: `student_embeddings`, `learning_object_embeddings`
"""
struct Dot
    vectorize::Bool
end

Dot(vectorize::Bool = true) = Dot(vectorize)
(m::Dot)(x, y) = (m.vectorize ? dot.(x, y) : dot(x, y)) |> gpu_or_cpu
@functor Dot

"""
A model that holds `student` and `learning object embeddings`. This model computes the *inner dot product* of student and learning object, `r`.

* params: `student_embeddings`, `learning_object_embeddings`
"""
struct Sum
    dims::Integer
end

Sum(dims::Integer = 1) = Sum(dims)
(m::Sum)(x) = sum(x, dims = m.dims) |> gpu_or_cpu
@functor Sum

"""
A model that implements *neural factorization by concatenating* student and learning object embeddings instead of calculating the inner product of student and learning object embeddings.

* params: `student_embeddings`, `learning_object_embeddings`
"""
struct NMF
    student_embeddings
    learning_object_embeddings
end

NMF(n_students::Integer, n_learning_objects::Integer, embedding_size::Integer, nonnegative::Bool = false, bias::Bool = true, embeddings_initializer::Any = kaiming_normal, rng::AbstractRNGOrNothing = nothing) = NMF(Embedding(n_students, embedding_size, nonnegative, bias, embeddings_initializer, rng), Embedding(n_learning_objects, embedding_size,  nonnegative, bias, embeddings_initializer, rng))
(m::NMF)(x) = vcat(m.student_embeddings(x[1, :]), m.learning_object_embeddings(x[2, :])) |> gpu_or_cpu
@functor NMF

"""
A model that implements *neural collaborative filtering by concatenating* gmf and nmf embeddings instead of calculating the inner product of gmf and nmf embeddings.

* params: `gmf_embeddings`, `nmf_embeddings`
"""
struct NCF
    gmf
    nmf
end
(m::NCF)(x) = vcat(m.gmf(x), m.nmf(x))
@functor NCF

"""
A model that implements *neural factorization by concatenating* student and learning object embeddings instead of calculating the inner product of student and learning object embeddings.

* params: `in`, `out`, `normalize`, `dropout`, `p`, `embeddings_initializer`, `bias`, `rng`
"""
function _create_MLP(in::Integer = 1, out::Integer = 1, normalize::Bool = true, dropout::Bool = true, p::Float16 = 0.5, embeddings_initializer::Any = kaiming_normal, bias = true, rng::AbstractRNGOrNothing = nothing)
    activation = out > 1 ? softmax : sigmoid
    layers = []
    batchnorm_layer = BatchNorm(in)
    dropout_layer = Dropout(p)

    if normalize push!(layers, batchnorm_layer) end
    if dropout push!(layers, dropout_layer) end

    rng = copy(rng)
    seed!(rng)

    Chain(
        Dense(in, in, identity, bias = bias, init = embeddings_initializer),
        layers...,
        Dense(in, in, relu, bias = bias, init = embeddings_initializer),
        layers...,
        Dense(in, in, relu, bias = bias, init = embeddings_initializer),
        layers...,
        Dense(in, out, activation, bias = bias, init = embeddings_initializer)
    ) |> gpu_or_cpu
end

function Auto_Encoder()
    # Latent dimensionality, # hidden units.
    a, b, c, d, e, f = n_cols(X_train), 1024, 512, 256, 64, 32

    println(n_cols(X_train), " ", size(X_train))

    encoder_layer_1 = Dense(a, b, relu)
    encoder_layer_2 = Dense(b, c, relu)
    encoder_layer_3 = Dense(c, d, relu)
    encoder_layer_4 = Dense(d, e, relu)
    encoder_layer_5 = Dense(e, f, relu)

    decoder_layer_1 = Dense(f, e, relu)
    decoder_layer_2 = Dense(e, d, relu)
    decoder_layer_3 = Dense(d, c, relu)
    decoder_layer_4 = Dense(c, b, relu)
    decoder_layer_5 = Dense(b, a, σ)

    # encoder
    encoder = Chain(
        encoder_layer_1,
        encoder_layer_2,
        encoder_layer_3,
        encoder_layer_4,
        encoder_layer_5,
    )

    # decoder
    decoder = Chain(
        decoder_layer_1,
        decoder_layer_2,
        decoder_layer_3,
        decoder_layer_4,
        decoder_layer_5,
    )

    # autoencoder
    autoencoder = Chain(encoder, decoder)
    autoencoder
end

struct EnsembleMF
    student_embeddings
    learning_object_embeddings
end

EnsembleMF(n_students::Integer, n_learning_objects::Integer, embedding_size::Integer, nonnegative::Bool = false, embeddings_initializer = kaiming_normal, rng::AbstractRNGOrNothing = nothing) = EnsembleMF(Embedding(n_students, embedding_size, nonnegative, embeddings_initializer, rng), Embedding(n_learning_objects, embedding_size,  nonnegative, embeddings_initializer, rng))
(m::EnsembleMF)(x) = (dot.(m.student_embeddings(x[1, :]), m.learning_object_embeddings(x[2, :]))) |> gpu_or_cpu
@functor EnsembleMF

end