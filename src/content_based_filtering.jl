module CBF

export KDTree, recommend, recommend_with_dr, knn, fit

import DataFrames: AbstractDataFrame, ByRow, DataFrame, combine, groupby, innerjoin, nrow, Not, order, reduce, select, select!, sort, transform!, vcat
import Distances: CorrDist, CosineDist, Euclidean, evaluate

import ManifoldLearning: Isomap, transform
import ManifoldLearning
import MultivariateStats
import NearestNeighbors

import StatsBase
import Statistics: cor
import TextAnalysis: tf_idf

import ..DataPattern: DistanceMetric, correlation, cosine, euclidean, pearson

@enum LDRAlgorithm pca_algo
@enum NLDRAlgorithm isomap_algo

struct KDTree <: ManifoldLearning.AbstractNearestNeighbors
    k::Integer
    fitted::NearestNeighbors.KDTree
end

Base.show(io::IO, NN::KDTree) = print(io, "KDTree(k=$(NN.k))")
StatsBase.fit(::Type{KDTree}, X::AbstractMatrix{T}, k::Integer) where {T<:Real} = KDTree(k, NearestNeighbors.KDTree(X))

function ManifoldLearning.knn(NN::KDTree, X::AbstractVecOrMat{T}; self=false) where {T<:Real}
    m, n = size(X)
    k = NN.k
    @assert n > k "Number of observations must be more then $(k)"

    idxs, dist = NearestNeighbors.knn(NN.fitted, X, k+1, true)
    D = Array{T}(undef, k, n)
    E = Array{Int32}(undef, k, n)
    for i in eachindex(idxs)
        E[:, i] = idxs[i][2:end]
        D[:, i] = dist[i][2:end]
    end
    return D, E
end



# function knn(NN::BruteForce{T}, X::AbstractVecOrMat{T}; self=false) where T<:Real
#     m, n = size(X)
#     k = NN.k
#     @assert n > k "Number of observations must be more then $(k)"

#     D = pairwise((x,y)->norm(x-y), NN.fitted, X)

#     d = Array{T}(undef, k, n)
#     e = Array{Int}(undef, k, n)
#     idxs = self ? (1:k) : (2:k+1)

#     @inbounds for j = 1 : n
#         e[:, j] = sortperm(D[:,j])[idxs]
#         d[:, j] = D[e[:, j],j]
#     end

#     return (d, e)
# end

function reduce_dimensionality(student_df::DataFrame, learning_object_df::DataFrame, embedding_k::Integer = 12, embedding_output_dimensions::Integer = 2, use_tf::Bool = true, use_non_linear_dimensionality_reduction::Bool = true)
    n_learning_objects = nrow(learning_object_df)
    df = reduce(vcat, [student_df, learning_object_df], cols = :union, source = nothing)

    X = Matrix(df)' |> Matrix

    X = use_tf ? tf_idf(X')' |> Matrix : X

    algorithm = use_non_linear_dimensionality_reduction ? Isomap : MultivariateStats.PCA

    M = use_non_linear_dimensionality_reduction ? StatsBase.fit(Isomap, X, nntype = KDTree; k = embedding_k, maxoutdim = embedding_output_dimensions) : MultivariateStats.fit(algorithm, X; maxoutdim = embedding_output_dimensions, method = :auto)
    
    println("X1 = $(X[:, 1])")
    println("M = $(M)")

    Y = use_non_linear_dimensionality_reduction ? transform(M) : MultivariateStats.transform(M, X)

    println("Y = $(size(Y)), X = $(size(X))")

    l = Y[:, 1:n_learning_objects]
	s = Y[:, n_learning_objects + 1:end]

    l, s
end

function recommend(student_df::DataFrame, learning_object_df::DataFrame, k::Integer = 10, metric::DistanceMetric = correlation, use_tf::Bool = true)
    n_learning_objects = nrow(learning_object_df)
    
    k = k < 1 ? 1 : k
    k = k > n_learning_objects ? n_learning_objects : k

    learning_objects = Matrix(learning_object_df)
    student = Matrix(student_df)

    data = Matrix(vcat(learning_objects, student))
    data = use_tf ? tf_idf(data)' : data'

    points = data[:, 1:n_learning_objects]
    query_point = data[:, n_learning_objects + 1]

    if metric == correlation
        learning_object_df[!, :rating_predicted] = [evaluate(CorrDist(), points[:, idx], query_point) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == cosine
        learning_object_df[!, :rating_predicted] = [evaluate(CosineDist(), points[:, idx], query_point) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == euclidean
        tree1 = NearestNeighbors.BruteTree(points, Euclidean())
        idxs, dists = NearestNeighbors.knn(tree1, query_point, k, true)
        learning_object_df = learning_object_df[[idxs...], :]
        learning_object_df[!, :rating_predicted] = dists
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
    elseif metric == pearson
        learning_object_df[!, :rating_predicted] = [abs(cor(points[:, idx], query_point)) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = true)])
        learning_object_df = learning_object_df[1:k, :]
    end
    learning_object_df
end

function recommend_with_dr(student_df::DataFrame, learning_object_df::DataFrame, k::Integer = 10, metric::DistanceMetric = correlation, use_tf::Bool = true, embedding_k::Integer = 12, embedding_output_dimensions::Integer = 50, use_non_linear_dimensionality_reduction::Bool = true)
    @assert nrow(student_df) == 1 "More than one student was provided"

    n_learning_objects = nrow(learning_object_df)
    
    k = k < 1 ? 1 : k
    k = k > n_learning_objects ? n_learning_objects : k

    l, s = reduce_dimensionality(student_df, learning_object_df, embedding_k, embedding_output_dimensions, use_tf, use_non_linear_dimensionality_reduction)

    data = vcat(l' |> Matrix, s' |> Matrix)
    data = use_tf ? tf_idf(data)' : data'
    points = data[:, 1:n_learning_objects]
    query_point = data[:, n_learning_objects + 1]

    # points = l
    # query_point = s

    if metric == correlation
        learning_object_df[!, :rating_predicted] = [evaluate(CorrDist(), points[:, idx], query_point) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == cosine
        learning_object_df[!, :rating_predicted] = [evaluate(CosineDist(), points[:, idx], query_point) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == euclidean
        tree1 = NearestNeighbors.BruteTree(points, Euclidean())
        idxs, dists = knn(tree1, query_point, k, true)
        learning_object_df = learning_object_df[[idxs...], :]
        learning_object_df[!, :rating_predicted] = dists
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
    elseif metric == pearson
        learning_object_df[!, :rating_predicted] = [abs(cor(points[:, idx], query_point)) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = true)])
        learning_object_df = learning_object_df[1:k, :]
    end
    learning_object_df
end

function recommend(student::AbstractArray, learning_objects::AbstractArray, learning_object_df::DataFrame, k::Integer = 10, metric::DistanceMetric = correlation, use_tf::Bool = true)
    @assert size(student, 1) == 1 "More than one student was provided"

    n_learning_objects = nrow(learning_object_df)

    @assert size(learning_objects, 1) == n_learning_objects "The total number of learning objects do not match provided DataFrame"
    
    k = k < 1 ? 1 : k
    k = k > n_learning_objects ? n_learning_objects : k

    data = vcat(learning_objects' |> Matrix, student' |> Matrix)
    data = use_tf ? tf_idf(data)' : data'
    points = data[:, 1:n_learning_objects]
    query_point = data[:, n_learning_objects + 1]

    if metric == correlation
        learning_object_df[!, :rating_predicted] = [evaluate(CorrDist(), points[:, idx], query_point) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == cosine
        learning_object_df[!, :rating_predicted] = [evaluate(CosineDist(), points[:, idx], query_point) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
        learning_object_df = learning_object_df[1:k, :]
    elseif metric == euclidean
        tree1 = NearestNeighbors.BruteTree(points, Euclidean())
        idxs, dists = NearestNeighbors.knn(tree1, query_point, k, true)
        learning_object_df = learning_object_df[[idxs...], :]
        learning_object_df[!, :rating_predicted] = dists
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = false)])
    elseif metric == pearson
        learning_object_df[!, :rating_predicted] = [abs(cor(points[:, idx], query_point)) for idx in 1:n_learning_objects]
        learning_object_df = sort(learning_object_df, [order(:rating_predicted, rev = true)])
        learning_object_df = learning_object_df[1:k, :]
    end
    learning_object_df
end

end