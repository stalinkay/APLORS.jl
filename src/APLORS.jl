module APLORS

include("model/layers.jl")
include("model/models.jl")
include("model/data.jl")

export Layers
export Models
export DataPattern

include("content_based_filtering.jl")
export CBF

include("collaborative_filtering.jl")

end # module