#!/usr/bin/env bash

if [ "$(uname)" == "Darwin" ]; then
    # Do something under Mac OS X platform
    export JULIA_MPI_PATH="/usr/local/Cellar/open-mpi/4.1.1_2"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # Do something under GNU/Linux platform
    export JULIA_MPI_PATH="/opt/ohpc/pub/mpi/openmpi3-gnu8/3.1.2"
else
    export JULIA_MPI_PATH="/opt/ohpc/pub/mpi/openmpi3-gnu8/3.1.2"
fi