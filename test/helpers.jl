# using CSV
# using DataFrames
# using MLDataUtils
# using PrettyTables
# using Random

# using ScikitLearn
# @sk_import preprocessing:LabelEncoder

# expert_encoder = LabelEncoder()
# expert_learning_object_encoder = LabelEncoder()
# expert_rating_encoder = LabelEncoder()

# Random.seed!(7)

# ### Helpers
# read_data_frame(x) = CSV.File(x) |> DataFrame!

# cleanup_column(c) = map(
#     (r) -> split(
#         replace(
#             replace(
#                 replace(replace(replace(r, " and" => ","), " or" => ","), "(" => ""),
#                 ")" => "",
#             ),
#             " " => "",
#         ),
#         ",",
#     ),
#     c,
# )

# convert_to_int64_array(a) = map((i) -> parse(Int64, i), a)

# convert_to_array(c) = map((r) -> r == "" ? [] : convert_to_int64_array(split(r, ",")), c)

# ### All Course Enrollments
# function get_all_course_enrollments()
#     ce_df = read_data_frame("dataset/v2/studentCourseEnrollments.csv")
#     return ce_df
# end

# ### All Topic Assessments
# function get_all_topic_assessments()
#     ta_df = read_data_frame("dataset/v2/studentTopicAssessments.csv")
#     ta_df[!, :percentage] = ta_df[!, :score] ./ ta_df[!, :total] .* 100
#     return ta_df
# end

# ### All Preferred Topics
# function get_all_preferred_topics()
#     pt_df = read_data_frame("dataset/v2/studentPreferredTopics.csv")
#     return pt_df
# end

# # All Topics
# function get_all_topics()
#     t_df = read_data_frame("dataset/v2/topics.csv")
#     t_df[!, :preRequisites] = coalesce.(t_df[!, :preRequisites], "") # convert null values to empty strings
#     t_df[!, :preRequisites] = convert_to_array(t_df[!, :preRequisites])
#     return t_df
# end

# # All Student Ratings
# function get_astudent_learning_object_ratings(samples = 16_378_549, split = 0.9)
#     rating_data = read_data_frame("dataset/v2/studentLearningObjectRatings.csv")
#     rating_data = rating_data[[:studentId, :learningObjectId, :rating]]
#     rating_data = rating_data[shuffle(axes(rating_data, 1)), :]
#     rating_data = rating_data[1:samples, :]

#     student_encoder = LabelEncoder()
#     student_learning_object_encoder = LabelEncoder()
#     student_rating_encoder = LabelEncoder()

#     rating_data[:studentId] = fit_transform!(student_encoder, rating_data[:studentId])
#     rating_data[:learningObjectId] =
#         fit_transform!(student_encoder, rating_data[:learningObjectId])

#     n_students = maximum(unique(rating_data[:studentId])) + 1 # add 1 to prevent BoundsError, Julia is 1 indexed, LabelEncoder starts from 0
#     n_student_learning_objects = maximum(unique(rating_data[:learningObjectId])) + 1 # add 1 to prevent BoundsError, Julia is 1 indexed, LabelEncoder starts from 0
#     println("students: ", n_students, " Learning Objects: ", n_student_learning_objects)
#     n_students = maximum([1, n_students])
#     n_student_learning_objects = maximum([1, n_student_learning_objects])
#     println("students: ", n_students, " Learning Objects: ", n_student_learning_objects)

#     student_matrix =
#         fill(0, (maximum([1, n_students]), maximum([1, n_student_learning_objects])))
#     pretty_table(student_matrix)

#     [
#         student_matrix[
#             rating_data[idx, :][:studentId]+1,
#             rating_data[idx, :][:learningObjectId]+1,
#         ] = rating_data[idx, :][:rating] for idx = 1:size(rating_data, 1)
#     ] # add 1 to prevent BoundsError, Julia is 1 indexed

#     X = student_matrix'' # Transpose back

#     # We leave out 30% of the data for testing
#     # (X_train, y_train), (X_test, y_test) = splitobs((X, X); at = split)

#     # X = gpu.(data)

#     splitobs((X, X); at = split) # returns (X_train, y_train), (X_test, y_test)
# end

# # All Expert Ratings
# function get_expert_learning_object_ratings(samples = 5_110_310, split = 0.9)
#     rating_data = read_data_frame("dataset/v2/expertLearningObjectRatings.csv")
#     rating_data = rating_data[[:expertId, :learningObjectId, :rating]]
#     rating_data = rating_data[shuffle(axes(rating_data, 1)), :]
#     rating_data = rating_data[1:samples, :]

#     expert_encoder = LabelEncoder()
#     expert_learning_object_encoder = LabelEncoder()
#     expert_rating_encoder = LabelEncoder()

#     rating_data[:expertId] = fit_transform!(expert_encoder, rating_data[:expertId])
#     rating_data[:learningObjectId] =
#         fit_transform!(expert_encoder, rating_data[:learningObjectId])

#     n_experts = maximum(unique(rating_data[:expertId])) + 1 # add 1 to prevent BoundsError, Julia is 1 indexed, LabelEncoder starts from 0
#     n_expert_learning_objects = maximum(unique(rating_data[:learningObjectId])) + 1 # add 1 to prevent BoundsError, Julia is 1 indexed, LabelEncoder starts from 0
#     println("Experts: ", n_experts, " Learning Objects: ", n_expert_learning_objects)
#     n_experts = maximum([1, n_experts])
#     n_expert_learning_objects = maximum([1, n_expert_learning_objects])
#     println("Experts: ", n_experts, " Learning Objects: ", n_expert_learning_objects)

#     expert_matrix =
#         fill(0, (maximum([1, n_experts]), maximum([1, n_expert_learning_objects])))
#     pretty_table(expert_matrix)

#     [
#         expert_matrix[
#             rating_data[idx, :][:expertId]+1,
#             rating_data[idx, :][:learningObjectId]+1,
#         ] = rating_data[idx, :][:rating] for idx = 1:size(rating_data, 1)
#     ] # add 1 to prevent BoundsError, Julia is 1 indexed

#     X = expert_matrix'' # Transpose back

#     # We leave out 30% of the data for testing
#     # (X_train, y_train), (X_test, y_test) = splitobs((X, X); at = split)

#     # X = gpu.(data)

#     splitobs((X, X); at = split) # returns (X_train, y_train), (X_test, y_test)
# end
